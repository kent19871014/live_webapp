# React-Redux Boilerplate
> Inspired by https://github.com/workco/marvin

[TOC]

## Getting Started

> Need Nodejs (>6.9.x)

### Installation

> npm i

### Start

> npm start

    how to run in local

    1. put game in `source/release`
    2. modify `gameClientUrl: ./release/web/index.html` in settings.json

### Test

> npm test

## File Struncture

```
Home                                # Base Path
├── __tests__                       # Folder of tests
    ├── setup                       # Unit test setup
    └── components                  # Tests for React components
└── source
    ├── style                       # Styles for icon font
    ├── scss                        # Styles
        ├── theme                   # for product theme
    └── configs                     # Pretend to be API
    └── js
        ├── actions
        ├── api
        ├── components              # React Components
        ├── dev                     # Logger
        ├── reducers
        ├── reducer
        ├── views                   # React Views that may including many components
        ├── index.js                # Entry point
        └── routes.js
    └── index.html
        
```
## settings.json
```
{
    "rtmpHost": "172.16.201.103",
    "gameApiBasePath": "http://172.16.201.13:8002",
    "hostApiBasePath": "http://172.16.201.103:8000",
    "wsHost": "172.16.201.103",
    "wsPort": "8001",
    "wsProtocol": "ws",
    "videoBasePath": "http://de1-stream.xyz858.com",
    "chatRoom": {
        "service": "ws://172.16.201.20:5280/ws",
        "domain": {
            "forReal": "@realgamehost",
            "forFun": "@fungamehost"
        },
        "groupChatAddr": "live_firedice_0@conference.realgamehost",
        "forFunChatName": "guest",
        "maxstanzas": 5,
        "seconds": 3600
    },
    "gameClientUrl": "./release/web/index.html",
    "historyClientUrl": "http://de1-cdn.allstar-interactive.com/games/History_Client/index.html#/Live",
    "wsGift": {
        "apihost": "172.16.201.13",
        "url": "/ws/v1/licensees/{licenseeID}/brands/{brandID}/games/{gameID}/subscription?type=gift&currencyCode={currency}&access_token={token}",
        "port": "7772",
        "protocol": "ws"
    },
    "giftAPI": {
        "host": "http://172.16.201.104:8002",
        "giftList": "/gift/{licenseeID}?brand={brandID}",
        "sendGift": "/donate/{licenseeID}/{giftId}/to/{receiverId}",
        "backPack": "/gift/pack/{licenseeID}",
        "sendBackPackGift": "/donate/pack/{licenseeID}/{missionGiftId}/to/{receiverId}",
        "hostInfo": "/giftAmount/mg/{hostId}",
        "updateGiftAmountTimeInterval": 10000
    },
    "giftCntPerPage": 8,
    "badwordsUrl": ["http://de1-cdn.allstar-interactive.com/host-game-assets/badwords/badword_base.txt", "http://de1-cdn.allstar-interactive.com/host-game-assets/badwords/badword_extend.txt"],
    "profileUrl": "http://de1-cdn.allstar-interactive.com/host-game-assets/profile/{externalId}/{externalId}.json",
    "lang": {
        "def": "en-us",
        "support": [
            "zh-cn",
            "en-us",
            "ja-jp",
            "ko-kr",
            "th-th",
            "vi-vn"
        ]
    },
    "gameSize": {
        "w": 750,
        "h": 616,
        "offsetH": 149
    },
    "nickname": {
        "max": 10,
        "min": 4
    },
    "boost": {
        "ruleImg": "http://de1-cdn.luckytwist.com/host-game-assets/static/013.png",
        "qeuryWSUrl": "ws://172.16.201.13:7772/ws/v1/licensees/{licenseeID}/brands/{brandID}/games/{gameID}/subscription?currencyCode={Currency}&access_token=",
        "patchNickname": "http://172.16.201.13:7772/api/v1/players/{playerId}"
    },
    "currencyConfigAPI": "http://172.16.201.13:7772/api/v1/currency-configs",
    "musicList": "http://de1-cdn.allstar-interactive.com/host-game-assets/music/musicList.json"
}
```
* `chatRoom/domain` - for chatroom chanel domain
* `groupChatAddr` - join group chat address
* `forFunChatName` - free play join group chatroom account
* `maxstanzas` - 最多顯示幾筆聊天紀錄 
* `seconds` - 顯示多久時間以前的聊天紀錄
* `gameClientUrl` - iframe client url
* `historyClientUrl` - iframe history url
* `wsGift` - gift manager notify websocket
* `giftAPI` - 禮物清單 & 送禮 API & 主播禮物資訊
* `updateGiftAmountTimeInterval` - 已經沒再用了可以忽略它
* `giftCntPerPage` - 禮物清單每頁顯示的筆數
* `badwordsUrl` - 屏蔽字檔案 query url
* `profileUrl` - 主播 profile query url
* `gameSize` - iframe game size from game client
    * `offsetH` - 高度誤差
* `nickname`
    * `max` - 暱稱最大長度
    * `min` - 暱稱最小長度
* `boost`
    * `ruleImg` - boost 規則頁的圖片 ps:目前沒用到，要等boost 2.0
    * `qeuryWSUrl` - boost websocket 與 slot game 同一個
    * `patchNickname` - 通知 boost 目前玩家暱稱，因為主播送boost禮物需要顯示玩家名稱
* `currencyConfigAPI` - curreny config 原本是要在boost rule裡用的，目前用不到
* `musicList` - Live 主播不再時要播放的背景音樂清單

## init flows

```flow
settings=>operation: Fetch settings.json
urlParam=>operation: get query string from url parameter
productStyle=>operation: import product style
initAccount=>operation: init user account info
stringTable=>operation: fetch string table
musicList=>operation: fetch music list
badwords=>operation: fetch bad words table
currencyConfig=>operation: query currency config
initBadWords=>operation: init bad words
layout=>operation: render layout template
cond=>condition: Yes or No?

settings->urlParam->productStyle->initAccount->stringTable->musicList->badwords->currencyConfig->initBadWords->layout
```

## streaming flow


```mermaid
graph TD

A[connect streaming server] -->B{Connected}
B -->|onStart| D[get host profile]
B -->|onPlaying| E[play video]
B -->|onEnd| F[stop vedio]
B -->|onError| A

I[fun live layout]

D --> G[render host profile]
E --> |show|I
F --> |hide|I


```

## chat room flow

```mermaid
graph TD

A[connect chat room server]
B{Connect state}
C[error message]
D{init}
E[input nickname]
F[patch nickname]
G[join group chat]
H[wait]
Z[guest join chat room]

A-->B
B-->|disconnect| A
B-->|auth fail| C
B-->|connect fail| C


D-->|has token|E
D-->|no token|Z

E-->F
F-->|connected|G
F-->|connect not yet|H
H-->|connected|G

```

## live boost flow 目前暫時沒有再用
```mermaid
graph TD

A{connect boost server}
B[onMessage]
C[init]
D[update]
E[reward]
F{loyaltyActive}
G[play boost anims]
H[power bar]
Z[onClose]

A-->|connected|B
A-->|disconnect|Z
Z-->|reconnect|A

B-->|LoyaltyAccumulatorBalance|C
B-->|LoyaltyAccumulatorBalanceUpdate|D
B-->|LoyaltyReward|E

D-->|update|H

E-->|receive message|F
F-->|enable|G
F-->|disable|ignore

```

## gift websocket flow
```mermaid
graph TD

A{connect gift server}
B(onMessage)
C[host gift amount]
D[like amount]
E[gift queue]
F(gift anims)
G[normal gift anims]
H[boost gift anims]
I[float animas]
J[fullscreen anims]

Z[onClose]

A-->|connected|B
A-->|disconnect|Z
Z-->|reconnect|A

B-->|update amount|C
C-->|render|D
B-->|update list|E
E-->F
F-->G
F-->H
H-->|current user|J
H-->|others|I
G-->I
```

## iframe game client and live web app flow
```mermaid
graph TD

A[main page]
B[loading page]
C[iframe game]
D[query string]
E[start page]
Z[play game]

A-->|loading|B
B-->|init|D
D-->|game id|C
C-->|loading completed|A
A-->|loading completed|E
E-->|click start button|A
A-->Z
```

## live webapp assets uploads
* dev env.
    * ftp path: `/var/www/html/cdn/de1-cdn.allstar-interactive.com/host-game-assets`

    ```
    ├ static            #gift static image
    ├ profile           #host josin profile
    ├ music             #background music & music list
    ├ badwords          #txt file, the bad words.json is old version
    ├ animation         #gift sprite sheet image for animation

    ```
* stage & prod env.
    * ftp path: `/var/www/html/cdn/host-game-assets/uploads`

    ```
    ├ static            #gift static image
    ├ profile           #host josin profile
    ├ music             #background music & music list
    ├ badwords          #txt file, the bad words.json is old version
    ├ animation         #gift sprite sheet image for animation

    ```

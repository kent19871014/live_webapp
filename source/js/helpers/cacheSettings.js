/**
 * Handle cached settings
 */
const name = 'cached-settings';

export const saveSettings = payload => {
    sessionStorage.setItem(name, JSON.stringify(payload));
};

export const getSettings = () => JSON.parse(sessionStorage.getItem(name)) || undefined;


import BadWords from "bad-words-chinese";
import store from "../store";

let badWords

export const initBadWords = () => {
    const {
        app
    } = store.getState();
    const badsWordsTable = app.get("badsWordsTable");
    const table = badsWordsTable ? badsWordsTable.toJSON() : {};

    badWords = new BadWords({
        englishList: table,
        chineseList: table
    })
}

export const cleanBadWords = (str) => {
    return badWords.clean(str);
}

export const replaceBetweenString = ({ str, start, end, what }) => {
    const range = Math.abs(end - start);

    let mark = "";

    for (let i = 0; i < range; i++) {
        mark += what;
    }

    return str.substring(0, start) + mark + str.substring(end);
}

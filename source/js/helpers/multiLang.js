import store from "../store";
import React, { Component } from 'react';

export function getMultiLangText (key) {
    const { app } = store.getState();
    const stringTable = app.get("stringTable");
    const accountInfo = app.get("accountInfo");
    const settings = app.get("settings").toJSON();
    const { def } = settings.lang;
    const { lang } = accountInfo.toJSON();
    const json = stringTable ? stringTable.toJSON() : {};
    const txt = json[key];

    if (txt === undefined) {
        return key;
    }
    else {
        return txt[lang] || txt[def];
    }
}

export function getMultiLangTextUI(key) {
    const txt = getMultiLangText(key);
    return (
        <span>
            {txt.replace(/\\u00A0/g, "\u00A0").split("\\n").map((item, key) => {
                return (
                    <p key={key}>{item}<br /></p>
                )
            })}
        </span>
    );
}

const unescapedChar = ["\\3a", "\\22", "\\26", "\\27", "\\2f", "\\3c", "\\3e", "\\5c", "\\20", "\\40"];
const escapedChar = [":", '"', "&", "'", "/", "<", ">", "\\", " ", "@"];

export const unescaped2Escaped = (str) => {
    return str.split("").map((s) => {
        const i = escapedChar.indexOf(s);
        return i === -1 ? s : unescapedChar[i];
    }).join("");
}

export const escaped2Unescaped = (str) => {
    return str.replace(/\\3a/, ":")
        .replace(/\\22/g, '"')
        .replace(/\\26/g, "&")
        .replace(/\\27/g, "'")
        .replace(/\\2f/g, "/")
        .replace(/\\3c/g, "<")
        .replace(/\\3e/g, ">")
        .replace(/\\5c/g, "\\")
        .replace(/\\20/g, " ")
}

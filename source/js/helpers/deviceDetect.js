const userAgent = navigator.userAgent;
let Sys = {};
let e;
let d;

Sys.isiPad = false;
Sys.isiPhone = false;
Sys.isiPod = false;
Sys.isAndroidDevice = false;
Sys.isAndroid23Device = false;
Sys.isAndroid400 = false;
Sys.isAndroid410 = false;
Sys.isAndroidTablet = false;
Sys.isAndroid3Tablet = false;
Sys.isDesktop = false;
Sys.isChrome = false;
Sys.isChrome280 = false;
Sys.isSafari = false;
Sys.isChromeForIOS = false;
Sys.isVivoBrowser = false;
Sys.isUCBrowser = false;
Sys.isOppoBrowser = false;
Sys.isSogouMobileBrowser = false;
Sys.isQQBrowser = false;
Sys.isFireFox = false;
Sys.isSupport = true;
Sys.isFireFoxForIOS = false
Sys.isGoodExperience = true;
Sys.core = "";

if (userAgent.match(/Chrome/i)) {
    Sys.isChrome = true;
    if (userAgent.match(/Chrome\/28[\.\d]/i)) {
        Sys.isChrome280 = true;
    }
}
if (userAgent.match(/UCBrowser/i)) {
    Sys.isUCBrowser = true;
}
if (userAgent.match(/OppoBrowser/i)) {
    Sys.isOppoBrowser = true;
}
if (userAgent.match(/SogouMobileBrowser/i)) {
    Sys.isSogouMobileBrowser = true;
}
if (userAgent.match(/QQBrowser/i)) {
    Sys.isQQBrowser = true;
}
if (userAgent.match(/Firefox/i)) {
    Sys.isFireFox = true;
}
if (userAgent.match(/FxiOS/i)) {
    Sys.isFireFoxForIOS = true;
}
if (userAgent.match(/CriOS/i)) {
    Sys.isChromeForIOS = true;
}
if (userAgent.match(/Safari/i) && !Sys.isChromeForIOS) {
    Sys.isSafari = true;
}
if (userAgent.match(/VivoBrowser/i)) {
    Sys.isVivoBrowser = true;
}
if (userAgent.match(/iPad/i) !== null) {
    Sys.isiPad = true;
} else {
    if ((userAgent.match(/iPod/i))) {
        Sys.isiPod = true;
    } else {
        if ((userAgent.match(/iPhone/i))) {
            e = "3gs,4,4s";
            d = "standard";
            e = (window.screen.height === 568) ? "5" : e;
            e = (window.screen.height === 667) ? "6" : e;
            d = window.matchMedia("(-webkit-min-device-pixel-ratio: 3)").matches && e === "6" ? "zoomed" : d;
            e = window.matchMedia("(-webkit-min-device-pixel-ratio: 3)").matches ? "6+" : e;
            Sys.isiPhone = {
                series: "iPhone",
                model: e,
                displayZoom: d
            }
        } else {
            if ((userAgent.match(/Android/i)) || userAgent.match(/HTC_Sensation/i)) {
                Sys.isAndroidDevice = true;
                if (userAgent.match(/Android 3[\.\d]+/i)) {
                    Sys.isAndroid3Tablet = true;
                    Sys.isAndroidTablet = true;
                } else {
                    if (!userAgent.match(/mobile/i)) {
                        Sys.isAndroidTablet = true;
                    } else {
                        if (userAgent.match(/Android 2\.3/i)) {
                            Sys.isAndroid23Device = true;
                        } else {
                            if (userAgent.match(/Android 4\.0/i)) {
                                Sys.isAndroid400 = true;
                            } else {
                                if (userAgent.match(/Android 4\.1/i)) {
                                    Sys.isAndroid410 = true;
                                } else {
                                    if (userAgent.match(/Android 4\.2/i)) {
                                        Sys.isAndroid420 = true;
                                    } else {
                                        if (userAgent.match(/Android 4\.3/i)) {
                                            Sys.isAndroid430 = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                Sys.isDesktop = true
            }
        }
    }
}
Sys.core = userAgent.match(/[^()]+(?=\))/)
Sys.isiOSDevice = Sys.isiPad || Sys.isiPhone || Sys.isiPod;
Sys.isMobile = Sys.isiOSDevice || Sys.isAndroidDevice;
if (Sys.isAndroidDevice) {
    if (Sys.core.toString().indexOf("Build") === -1 && Sys.core.toString().indexOf("U;") === -1) {
        Sys.isSupport = true;
    } else {
        Sys.isGoodExperience = false;
        Sys.isSupport = false;
    }
}
else if (Sys.isiOSDevice) {
    Sys.isSupport = Sys.isChromeForIOS || Sys.isFireFoxForIOS || (Sys.isSafari && !Sys.isQQBrowser);
    Sys.isGoodExperience = Sys.isSupport;
}
export default Sys;

import React, { Component } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { getMultiLangText } from "../../helpers/multiLang";

@connect(state => ({
}))

export default class FullScreenAnim extends Component {
    constructor(prop) {
        super(prop);
    }

    renderGiftMessage(giftType, from, to) {
        switch (giftType) {
            case "Mission":
                return this.renderMissionMessage(from, to);
            case "Boost":
                return this.renderBoostMessage(from, to);
        }
    }


    renderMissionMessage(from, to) {
        return (
            <div className="txt-box">
                <span className="user">{from.displayName}</span>
                <span className="gift-label">{getMultiLangText("Txt_Give") + " " + to.displayName}</span>
            </div>
        )
    }

    renderBoostMessage(from, to) {
        return (
            <div className="txt-box boost-gift">
            <div className="user">{getMultiLangText("Web_SLBST_000115").replace("{{host}}", from.displayName)}</div>
                <div className="gift-label">{getMultiLangText("Web_SLBST_000113").replace("{{player}}", to.displayName)}</div>
            </div>
        )
    }

    render() {
        const { giftType, gift, from, to, screenWidth, gameSize, handleGiftFullScreenAnimationEnd } = this.props;
        const aniUrl = gift.get('animationUrl');
        const gameHeight = (screenWidth / (gameSize.w / gameSize.h));

        let giftHeight = Math.round(window.innerHeight - gameHeight);

        if (giftHeight > screenWidth) {
            giftHeight = screenWidth;
        }

        return (
            <div className="gift-fullscreen-animation"
                onAnimationEnd={handleGiftFullScreenAnimationEnd}
                style={{ height: `${giftHeight}px`, width: `${giftHeight}px` }}
            >
                <div className="animation">
                    <i className="gift-item" style={{ "backgroundImage": `url(${aniUrl ? aniUrl : gift.get('iconUrl')})` }}></i>
                </div>
                {this.renderGiftMessage(giftType, from, to)}
            </div>
        )
    }
}

import React, { Component } from 'react';
import classNames from 'classnames';
import { getMultiLangText } from "../../helpers/multiLang";
import { connect } from 'react-redux';

@connect(state => ({
}))

export default class FloatAnim extends Component {
    constructor(prop) {
        super(prop);
    }

    renderUserMsg(giftType, textStyle, from, to, giftAmount) {
        switch (giftType) {
            case "Boost":
                return (
                    <div className={textStyle}>
                        <div className="user ellipsis">{getMultiLangText("Web_SLBST_000115").replace("{{host}}", from.displayName)}</div>
                        <div>
                            <span className="host ellipsis">{getMultiLangText("Web_SLBST_000114").replace("{{player}}", to.displayName)}</span>
                        </div>
                        <div className="free-spins-times">{getMultiLangText("Web_SLBST_000116").replace("{{times}}", giftAmount)}</div>
                    </div>
                )
            default:
                return (
                    <div className={textStyle}>
                        <div className="user ellipsis">{from.displayName}</div>
                        <div>
                            <span className="gift-label">{getMultiLangText("Txt_Give")}</span>
                            <span className="host ellipsis">{to.displayName}</span>
                        </div>
                    </div>
                )
        }
    }

    render() {
        const { currPlayingGiftAnimation, giftAmount, giftType, gift, from, to, screenWidth, gameSize, handleGiftAnimationEnd } = this.props;
        const bottom = "calc(" + (screenWidth / (gameSize.w / (gameSize.h - gameSize.offsetH)) + 52) + "px + 20%)";
        const style = classNames({
            'gift-animation': !!currPlayingGiftAnimation,
            'fadeaway-instantly': !gift.get('animationUrl'),
            "boost-gift": giftType === "Boost",
        });
        const aniUrl = gift.get('animationUrl');

        const giftAnimStyle = classNames({
            "animation": !!aniUrl && giftType !== "Boost",
            "static": !!!aniUrl || giftType === "Boost"
        })

        const textStyle = classNames({
            "txt-box": true,
            "boost-gift": giftType === "Boost",
        })


        return (
            <div className={style} onAnimationEnd={handleGiftAnimationEnd} style={{ bottom }}>
                {this.renderUserMsg(giftType, textStyle, from , to, giftAmount)}
                <div className="gift-item" >
                    <i className={giftAnimStyle} style={{ "backgroundImage": `url(${aniUrl && giftType !== "Boost" ? aniUrl : gift.get('iconUrl')})` }}></i>
                </div>
            </div>
        )
    }
}

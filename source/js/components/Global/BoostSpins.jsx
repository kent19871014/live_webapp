import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getMultiLangText } from "../../helpers/multiLang"
import { setLocalStorage } from "../../helpers/localstorage";

@connect(state => ({
    settings: state.app.get('settings'),
    isJoinGroupChat: state.chat.get("isJoinGroupChat"),
    screenSize: state.app.get("screenSize"),
    isActive: state.boost.get("isActive"),
    rewardType: state.boost.get("rewardType"),
}))

export default class BoostSpins extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFirst: true,
            spinsNotifyNoMoreShow: !!localStorage.getItem("spinsNotifyNoMoreShow")
        }
    }

    clickNoMoreShow() {
        setLocalStorage("spinsNotifyNoMoreShow", false);
    }

    render() {
        const { isJoinGroupChat, settings, screenSize, isActive, rewardType } = this.props;
        const { isFirst, spinsNotifyNoMoreShow } = this.state;

        // 加入群聊後才顯示
        if (!isActive || rewardType != "FreeSpins" || spinsNotifyNoMoreShow || !isJoinGroupChat || !isFirst) {
            return null;
        }

        const { gameSize } = settings.toJSON();
        const bottom = "calc(" + ((screenSize.toJSON().screenWidth / (gameSize.w / gameSize.h)) + 52) + "px + 5vh";

        window.setTimeout(() => {
            this.setState({ isFirst: false });
        }, 2000);

        return (
            <div className="free-spins-notifiy" style={{"bottom": bottom}}>
                <div className="spins-img">
                    <img src="./assets/img/common/boost-freespins.png" />
                </div>
                <div className="spins-msg">
                    <p>{getMultiLangText("Web_SLBST_000112")}</p>
                </div>
            </div>
        )
    }
}

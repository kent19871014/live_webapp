import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getMultiLangText } from "../../helpers/multiLang";
import classNames from 'classnames';
import { userErrorMsg } from 'actions/app';

@connect(state => ({
    userErrorMsg: state.app.get("userErrorMsg"),
}))

export default class UserErrorMessage extends Component {
    constructor(props) {
        super(props);
    }

    handleRemoveMsg = () => {
        const { dispatch } = this.props;
        dispatch(userErrorMsg({
            txt: "",
            errcode: -1
        }));
    }

    render() {
        const { userErrorMsg } = this.props;
        const { txt, errcode } = userErrorMsg.toJSON();
        const style = classNames({
            "about-balance": errcode === 3001,
            "user-error-msg": true,
            'active': !!txt,
        });

        if (txt) {
            setTimeout(this.handleRemoveMsg, 3000);
        }

        return (
            <div className={style}>
                <span>{txt ? txt : ""}</span>
            </div>
        )
    }
}

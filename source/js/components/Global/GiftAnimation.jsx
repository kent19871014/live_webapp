import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { removeReceiveGift, playGiftAnimation } from "actions/app";
import { getMultiLangText } from "../../helpers/multiLang";
import FullScreenAnim from "../Gift/FullScreenAnim";
import FloatAnim from "../Gift/FloatAnim";

@connect(state => ({
    settings: state.app.get('settings'),
    allGiftList: state.gift.get("allGiftList"),
    giftList: state.gift.get("giftList"),
    currPlayingGiftAnimation: state.gift.get("currPlayingGiftAnimation"),
    receiveGiftList: state.gift.get("receiveGiftList"),
    screenSize: state.app.get("screenSize"),
    accountInfo: state.app.get("accountInfo"),
}))

export default class GiftAnimation extends Component {
    constructor(prop) {
        super(prop);
    }

    handleGiftAnimationEnd(event) {
        const { dispatch } = this.props;
        event.currentTarget.classList.remove("gift-animation");
        window.setTimeout(() => {
            dispatch(removeReceiveGift());
        }, 100);
    }

    handleGiftFullScreenAnimationEnd(event) {
        const { dispatch } = this.props;
        event.currentTarget.classList.remove("gift-fullscreen-animation");
        window.setTimeout(() => {
            dispatch(removeReceiveGift());
        }, 100);
    }

    render() {
        const { dispatch, currPlayingGiftAnimation, allGiftList, giftList, receiveGiftList, screenSize, settings, accountInfo } = this.props;
        const { screenWidth, screenHeight } = screenSize.toJSON();
        const { gameSize } = settings ? settings.toJSON() : {};
        const bottom = "calc(" + (screenWidth / (gameSize.w / (gameSize.h - gameSize.offsetH)) + 52) + "px + 20%)";

        // 沒有禮物動畫時
        if (!currPlayingGiftAnimation) {
            if (receiveGiftList.size !== 0) {
                dispatch(playGiftAnimation(receiveGiftList.get(0)));
            }
            const gameHeight = (screenWidth / (gameSize.w / gameSize.h));

            let giftHeight = Math.round(window.innerHeight - gameHeight);

            if (giftHeight > screenWidth) {
                giftHeight = screenWidth;
            }

            return (
                <div style={{ display: 'none' }}>
                    <div className='txt-box'>
                        <div className='user'></div>
                        <div className='txt'>
                            <span>{getMultiLangText("Txt_Give")}</span>
                            <span className="host"></span>
                        </div>
                    </div>
                </div>
            );
        }

        const {
            ID,
            from,
            giftID,
            to,
            animationType,
            giftAmount,
        } = currPlayingGiftAnimation ? currPlayingGiftAnimation.toJSON() : {};



        // float animation
        if (animationType === 0) {
            const gift = giftList.filter(gift => gift.get('giftId') === giftID).get(0);
            return (
                <FloatAnim giftType="Mission"
                    gift={gift}
                    from={from}
                    to={to}
                    giftAmount={giftAmount}
                    screenWidth={screenWidth}
                    gameSize={gameSize}
                    currPlayingGiftAnimation={currPlayingGiftAnimation}
                    handleGiftAnimationEnd={this.handleGiftAnimationEnd.bind(this)}
                />
            )

        }
        else if (animationType === 1) { // full screen animation
            const gift = allGiftList.filter(gift => gift.get('giftId') === giftID).get(0);

            return (
                <FullScreenAnim giftType="Mission"
                    gift={gift}
                    from={from}
                    to={to}
                    giftAmount={giftAmount}
                    screenWidth={screenWidth}
                    gameSize={gameSize}
                    handleGiftFullScreenAnimationEnd={this.handleGiftFullScreenAnimationEnd.bind(this)}
                />
            )
        }
        else { // boost
            const { userId } = accountInfo.toJSON();
            const gift = allGiftList.filter(gift => gift.get('giftId') === giftID).get(0);

            // 如果是自己動畫要full screen的
            if (to.userID === userId) {
                return (
                    <FullScreenAnim
                        giftType="Boost"
                        gift={gift}
                        from={from}
                        to={to}
                        giftAmount={giftAmount}
                        screenWidth={screenWidth}
                        gameSize={gameSize}
                        handleGiftFullScreenAnimationEnd={this.handleGiftFullScreenAnimationEnd.bind(this)}
                    />
                )
            }
            else {
                return (
                    <FloatAnim
                        giftType="Boost"
                        gift={gift}
                        from={from}
                        to={to}
                        giftAmount={giftAmount}
                        screenWidth={screenWidth}
                        gameSize={gameSize}
                        currPlayingGiftAnimation={currPlayingGiftAnimation}
                        handleGiftAnimationEnd={this.handleGiftAnimationEnd.bind(this)}
                    />
                )
            }
        }
    }
}

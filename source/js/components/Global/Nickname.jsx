import React, { Component } from 'react';
import classNames from 'classnames';
import emoji from 'emoji-regex';
import { saveNickname } from 'actions/app';
import { connect } from 'react-redux';
import {
    cleanBadWords,
} from "helpers/badWords";
import { getMultiLangText } from "../../helpers/multiLang";


@connect(state => ({
    isSaveNickname: state.chat.get("isSaveNickname"),
    accountInfo: state.app.get("accountInfo"),
    settings: state.app.get("settings"),
    nickname: state.chat.get("nickname"),
}))
export default class Nickname extends Component {
    constructor(props) {
        super(props);

        this.inputVal = "";
        this.isOnComposition = false;
        this.state = {
            pass: false,
            isFirst: true,
        }
        this.unpassMsg = "";
    }

    componentDidMount() {
        const { nickname } = this.props;

        if (this.elemInput) {
            this.elemInput.value = nickname;
            this.inputVal = nickname;
            this.setState({
                pass: this.checkNicknameLength()
            })
        }
    }

    checkNicknameLength = () => {
        const { settings } = this.props;
        const { nickname } = settings.toJSON();
        const { min, max } = nickname;
        const isPass = this.inputVal.length >= min && this.inputVal.length <= max;

        if (!isPass) {
            this.unpassMsg = "Txt_Hint_05";
        }

        return isPass;
    }

    checkNicknameBadWords = () => {
        const words = cleanBadWords(this.inputVal);
        const isBadWords = words != this.inputVal && words.indexOf("*") != -1;

        if (isBadWords) {
            this.unpassMsg = "Err_ChatMsg_01";
        }

        return !isBadWords;
    }

    handleComposition(event) {
        if (event.type === 'compositionend') {
            this.isOnComposition = false;
            this.handleInputChange(event);
        }
        else {
            // in composition
            this.isOnComposition = true;
        }
    }

    handleInputChange(event) {
        if (event.target instanceof HTMLInputElement && !this.isOnComposition) {
            this.inputVal = event.target.value;
            this.inputVal = this.inputVal.replace(emoji(), '');

            if(!this.checkNicknameLength()) {
                this.setState({
                    pass: false,
                    isFirst: false
                });

                return;
            }
            else if(!this.checkNicknameBadWords()) {
                this.setState({
                    pass: false,
                    isFirst: false
                });

                return;
            }
            else {
                this.setState({
                    pass: true,
                    isFirst: false
                });
            }

        }
    }

    handleBtnClick = () => {
        const { dispatch } = this.props;
        const { pass } = this.state;

        if (!pass) {
            return;
        }
        dispatch(saveNickname(this.inputVal, true))
    }

    handleKeyEvent(event) {
        if (event.keyCode === 13) {
            this.handleBtnClick();
        }
        else if (event.keyCode === 65) {
            this.isOnComposition = false;
            this.handleInputChange(event);
        }
    }

    render() {
        const { isSaveNickname, accountInfo, settings } = this.props;
        const { token, externalId } = accountInfo.toJSON();
        const { nickname } = settings.toJSON();
        const { max } = nickname
        const { pass, isFirst } = this.state;
        const iconStyle = classNames({
            "icon-checked": !isFirst && pass,
            "icon-wrong": !isFirst && !pass,
        });

        const nameBoxStyle = classNames({
            "name-box": true,
            "wrong": !isFirst && !pass
        });

        const btnStyle = classNames({
            btn: true,
            active: pass,
        });

        if (isSaveNickname || !token) {
            return null;
        }

        return (
            <div className="nickname">
                <div className="mask"></div>
                <div className="wrapper">
                    <div className="tip">
                        {getMultiLangText("Txt_Hint_04")}
                    </div>
                    <div className={nameBoxStyle}>
                        <div className="icon">
                        </div>
                        <input type="text" className="name" placeholder={getMultiLangText("Txt_Input")}
                            ref={el => this.elemInput = el}
                            onChange={this.handleInputChange.bind(this)}
                            onCompositionStart={this.handleComposition.bind(this)}
                            onCompositionUpdate={this.handleComposition.bind(this)}
                            onCompositionEnd={this.handleComposition.bind(this)}
                            onKeyUp={this.handleKeyEvent.bind(this)}
                            maxLength={max}
                        />
                        <div className="checked-box">
                            <span className={iconStyle}>
                                <span className="path1"></span>
                                <span className="path2"></span>
                            </span>
                        </div>
                    </div>
                    <div className="wrong-msg">
                        <span style={{ "display": (!isFirst && !pass ? "block" : "none") }}>{getMultiLangText(this.unpassMsg)}</span>
                    </div>
                    <div className={btnStyle} onClick={this.handleBtnClick}>
                        <span>{getMultiLangText("Txt_Botton_02")}</span>
                    </div>
                </div>
            </div>
        )
    }
}

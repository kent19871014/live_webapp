import React, { Component } from 'react';
import classNames from 'classnames';
import { openUI, openSounds, openMusic, sendGift, muteGameMusic } from 'actions/app';
import { connect } from 'react-redux';
import {
    OPEN_SUB_MENU_BAR,
    OPEN_CHAT_INPUT_BOX,
    OPEN_GIFT_LIST,
    OPEN_GIFT_HISTORY,
    OPEN_RULE,
} from 'actions/type';
import MenuBarItem from 'components/Global/MenuBarItem';

@connect(state => ({
    accountInfo: state.app.get("accountInfo"),
    hostAccount: state.host.get("hostAccount"),
    isOpenSubMenuBar: state.app.get("isOpenSubMenuBar"),
    isOpenChatInputText: state.app.get("isOpenChatInputText"),
    isOpenGiftList: state.gift.get("isOpenGiftList"),
    isMuteSounds: state.app.get("isMuteSounds"),
    isMuteMusic: state.app.get("isMuteMusic"),
    isMuteGameMusic: state.app.get("isMuteGameMusic"),
    isOpenGiftHistory: state.app.get("isOpenGiftHistory"),
    freeGiftList: state.gift.get('freeGiftList'),
    isPlayingVideo: state.app.get('isPlayingVideo'),
    isOpenRule: state.app.get('isOpenRule'),
    displayNewGiftTip: state.gift.get('displayNewGiftTip'),
    isBoostGiftReward: state.gift.get('isBoostGiftReward'),
}))

export default class MenuBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openMenubar: false,
            isBoomAnimCompleted: true,
            isPlayHeartBeatAnim: false,
            isSubMenuActive: false,
            isPlayGiftIconAnim: false,
        }

    }

    componentWillReceiveProps(nextProps) {
        const { isBoostGiftReward } = nextProps;
        const { isBoomAnimCompleted } = this.state;

        if (isBoostGiftReward && isBoomAnimCompleted) {
            this.setState({
                isBoomAnimCompleted: false,
            })
        }
    }

    /**
     * menu event
     */
    handleSubMenuBarShow = () => {
        const { dispatch } = this.props;

        this.setState({
            isSubMenuActive: true
        })

        dispatch(openUI(OPEN_SUB_MENU_BAR, true));
    }

    /**
     * close other ui from menu bar open
     */
    handleUIDisplay = (type) => {
        const { dispatch, isOpenGiftList, isOpenRule, isOpenGiftHistory, isOpenChatInputText, isOpenSubMenuBar } = this.props;

        if (isOpenGiftList && type !== OPEN_GIFT_LIST) {
            dispatch(openUI(OPEN_GIFT_LIST, false));
        }

        if (isOpenSubMenuBar && type === OPEN_SUB_MENU_BAR) {
            dispatch(openUI(OPEN_SUB_MENU_BAR, false));
        }

        if (isOpenGiftHistory && type !== OPEN_GIFT_HISTORY) {
            dispatch(openUI(OPEN_GIFT_HISTORY, false));
        }

        if (isOpenChatInputText && type !== OPEN_CHAT_INPUT_BOX) {
            dispatch(openUI(OPEN_CHAT_INPUT_BOX, false));
        }
        if (isOpenRule && type !== OPEN_RULE) {
            dispatch(openUI(OPEN_RULE, false));
        }
    }

    /**
     * keyboard
     */
    handleKeyboardDisplay = () => {
        const { dispatch, isOpenChatInputText } = this.props;

        this.handleUIDisplay(OPEN_CHAT_INPUT_BOX);
        dispatch(openUI(OPEN_CHAT_INPUT_BOX, !isOpenChatInputText));
    }

    /**
     * gift list
     */
    handleSwitchToGiftList = e => {
        const { dispatch, isOpenGiftList } = this.props;

        this.setState({
            isPlayGiftIconAnim: true,
        })

        dispatch(openUI(OPEN_GIFT_LIST, !isOpenGiftList));
        this.handleUIDisplay(OPEN_GIFT_LIST);
    }

    onGiftIconAnimEnd = e => {
        this.setState({
            isPlayGiftIconAnim: false,
        })
    }

    handleGreeting = e => {
        const { dispatch, hostAccount, freeGiftList } = this.props;
        const { sub, token: hostToken } = hostAccount ? hostAccount.toJSON() : {};
        // random free gift
        const randomList = (freeGiftList ? freeGiftList.toJS() : [])
            .sort((a, b) => {
                return Math.random() - 0.5;
            });

        if (randomList.length === 0) {
            console.error("no free gift");
            return;
        }

        const gift = randomList[0];
        dispatch(sendGift({
            giftId: gift.giftId,
            receiverId: sub || hostToken,
        }));
        this.handleUIDisplay();
        this.setState({
            isPlayHeartBeatAnim: true
        })
    }

    onHeartAnimEnd = e => {
        this.setState({
            isPlayHeartBeatAnim: false
        })
    }

    /**
     * switch sound mute
     */
    handleSwitchMuteSounds = e => {
        const { dispatch, isMuteSounds } = this.props;
        dispatch(openSounds(!isMuteSounds));

        this.handleUIDisplay();
    }

    /**
     * switch music mute
     */
    handleSwitchMuteMusic = e => {
        const { dispatch, isMuteMusic } = this.props;
        dispatch(openMusic(!isMuteMusic));
        this.handleUIDisplay();
    }

    /**
     * switch game music mute
     */
    handleSwitchMuteGameMusic = e => {
        const { dispatch, isMuteGameMusic } = this.props;
        dispatch(muteGameMusic(!isMuteGameMusic));
        this.handleUIDisplay();
    }

    /**
     * rule page
     */
    handleRulePageDisplay = () => {
        const { dispatch, isOpenRule } = this.props;

        if (isOpenRule) {
            return;
        }

        dispatch(openUI(OPEN_RULE, true));
        this.handleUIDisplay(OPEN_RULE);
    }

    handleClickHistory = e => {
        const { dispatch, isOpenGiftHistory } = this.props;

        if (isOpenGiftHistory) {
            return;
        }

        dispatch(openUI(OPEN_GIFT_HISTORY, true));
        this.handleUIDisplay(OPEN_GIFT_HISTORY);
    }

    handleBoomAnimEnd(event) {
        event.stopPropagation();
        event.preventDefault();

        this.setState({
            isBoomAnimCompleted: true
        })
    }

    onCloseAnimEnd = e => {
        this.handleUIDisplay(OPEN_SUB_MENU_BAR);
        this.setState({
            isSubMenuActive: false
        })
    }

    render() {
        const { isOpenSubMenuBar, accountInfo, isPlayingVideo, isOpenGiftList, isOpenRule, isOpenGiftHistory, isOpenChatInputText, displayNewGiftTip, isBoostGiftReward, isMuteMusic, isMuteGameMusic, isMuteSounds } = this.props;
        const { token } = accountInfo.toJSON();
        const { isBoomAnimCompleted, isPlayHeartBeatAnim, isSubMenuActive, isPlayGiftIconAnim } = this.state;

        const mainMenuStyle = classNames({
            "main-menu": true,
        });

        const subMenuStyle = classNames({
            'sub-menu': true,
            'active': isOpenSubMenuBar && isSubMenuActive,
        });

        const boomStyle = classNames({
            "boom": true,
            "icon-gift": true,
            "active": isBoostGiftReward && !isBoomAnimCompleted
        })

        const heartStryle = classNames({
            "heartBeat": isPlayHeartBeatAnim,
        })

        const giftStyle = classNames({
            "btn": true,
            "icon-gift": true,
            "point": (displayNewGiftTip && isBoomAnimCompleted),
            "pulse": isPlayGiftIconAnim,
        })

        return (
            <div className='menubar-section'>
                <section className={mainMenuStyle}>
                    <MenuBarItem icon="close" handleClick={this.onCloseAnimEnd} style={{ "display": (isOpenGiftList || isOpenChatInputText) ? "unset" : "none" }}></MenuBarItem>
                    <MenuBarItem icon="menu" handleClick={this.handleSubMenuBarShow} style={{ "display": (isOpenGiftList || isOpenChatInputText) ? "none" : "unset" }}></MenuBarItem>
                    <MenuBarItem icon="message" disabled={!token} clickAnimName="pulse" active={isOpenChatInputText} handleClick={token ? this.handleKeyboardDisplay : null}></MenuBarItem>
                    <span className={giftStyle} onClick={this.handleSwitchToGiftList} onAnimationEnd={this.onGiftIconAnimEnd}>
                        <span className={boomStyle} onAnimationEnd={this.handleBoomAnimEnd.bind(this)}></span>
                    </span>
                    <MenuBarItem icon="heart-full" classStyle={heartStryle} disabled={!(token && isPlayingVideo)} clickAnimName="heartBeat" handleClick={token && isPlayingVideo ? this.handleGreeting : null}></MenuBarItem>
                </section>
                <section className={subMenuStyle}>
                    <MenuBarItem icon="close" handleClick={this.onCloseAnimEnd}></MenuBarItem>
                    <MenuBarItem icon="game-rule" clickAnimName="pulse" active={isOpenRule} handleClick={this.handleRulePageDisplay}></MenuBarItem>
                    {/* <MenuBarItem icon={isMuteMusic ? "music-no" : "music"} disabled={isPlayingVideo} clickAnimName="pulse" handleClick={isPlayingVideo ? null : this.handleSwitchMuteMusic}></MenuBarItem> */}
                    <MenuBarItem icon={isMuteGameMusic ? "music-no" : "music"} clickAnimName="pulse" handleClick={this.handleSwitchMuteGameMusic}></MenuBarItem>
                    <MenuBarItem icon={isMuteSounds ? "sound-no" : "sound"} clickAnimName="pulse" handleClick={this.handleSwitchMuteSounds}></MenuBarItem>
                    <MenuBarItem icon="history" clickAnimName="pulse" active={isOpenGiftHistory} handleClick={this.handleClickHistory}></MenuBarItem>
                </section>
            </div>
        );
    }
}

import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';

import FireDiceRule from '../GameRule/FireDiceDeluxRule';
import DoubleCardsRule from '../GameRule/DoubleCardsRule';
import FireDiceTurboRule from '../GameRule/FireDiceTurboRule';
import Live_QuadrupleDice from '../GameRule/Live_QuadrupleDice';
import WesternJourneyRule from '../GameRule/slot/WesternJourneyRule';
import SnakeLadyRule from '../GameRule/slot/SnakeLadyRule';
import ChineseNoodleRule from '../GameRule/slot/ChineseNoodleRule';
import Spin168Rule from '../GameRule/slot/Spin168Rule';
import JungleRule from '../GameRule/slot/JungleRule';

import { openUI } from 'actions/app';
import { OPEN_SUB_MENU_BAR, OPEN_RULE } from 'actions/type';

const ruleComponents = {
	Live_FireDice: FireDiceRule,
	Live_DoubleCards: DoubleCardsRule,
	Live_DoubleCardsTurbo: DoubleCardsRule,
	Live_PB_DoubleCards: DoubleCardsRule,
	Live_FireDiceTurbo: FireDiceTurboRule,
	Live_QuadrupleDice: Live_QuadrupleDice,
	Live_WesternJourney: WesternJourneyRule,
	Live_Jungle: JungleRule,
	Live_SnakeLady: SnakeLadyRule,
	Live_ChineseNoodle: ChineseNoodleRule,
	Live_Spin168: Spin168Rule,
};

@connect(state => ({
    settings: state.app.get('settings'),
	accountInfo: state.app.get('accountInfo'),
	isOpenRule: state.app.get('isOpenRule'),
}))


export default class Rule extends Component {
	constructor(props) {
		super(props);
		this.state = {
			display: false,
		};
	}

	componentWillReceiveProps(nextProps) {
		const { isOpenRule } = nextProps;

		if (isOpenRule) {
			this.setState({
				display: true,
			});
		}
	}

	handleHideRule = () => {
		const { dispatch } = this.props;

		dispatch(openUI(OPEN_RULE, false));
		dispatch(openUI(OPEN_SUB_MENU_BAR, false));
	}

    /**
     * 規則頁show / hide 動畫完成
     */
	handleTransitionEnd = () => {
		const { dispatch, isOpenRule } = this.props;

		if (!isOpenRule) {
			this.setState({
				display: false,
			});
		}
	}

	render() {
		const { accountInfo, isOpenRule, settings } = this.props;
		const { productId } = accountInfo.toJSON();
		const { gameRuleUrl } = settings.toJSON();
		const TagName = ruleComponents[productId];
		const component = TagName || !gameRuleUrl ? <TagName  productId={productId} /> : <iframe id="iframe-game-rule" name="gameRule" src={gameRuleUrl} />;
		const { display } = this.state;
		const style = classNames({
			'game-rule': true,
			'active': isOpenRule,
			'hidden': !display,
		});
		return (
			<div className={`${style} ${productId}`}>
				<div className='mask' onClick={this.handleHideRule} />
                <div className="wrapper" onTransitionEnd={this.handleTransitionEnd}>
				    {component}
                </div>
			</div>
		);
	}
}

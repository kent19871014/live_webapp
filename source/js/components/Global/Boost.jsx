import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { displayBoostRule, connectBoostServer, setBoostReward } from 'actions/app';

@connect(state => ({
    isActive: state.boost.get("isActive"),
    currPower: state.boost.get("currPower"),
    targetPower: state.boost.get("targetPower"),
    isReward: state.boost.get("isReward"),
}))

export default class Boost extends Component {

    componentDidMount() {
        connectBoostServer()
    }

    handleRule() {
        const { dispatch } = this.props;

        dispatch(displayBoostRule(true));
    }

    handleRewardAnimEnd() {
        const { dispatch } = this.props;

        dispatch(setBoostReward(false));
    }

    handleLightAnimEnd(event) {
        event.stopPropagation();
        event.preventDefault();
        this.playLightEl.style.animationName = "";
    }

    render() {
        return null;
        const { isActive, currPower, isReward, targetPower } = this.props

        if (!isActive) {
            return null;
        }

        const styles = classNames({
            body: true,
            win: isReward,
        })

        const width = ((currPower / targetPower) * 100);

        if (this.playLightEl) {
            this.playLightEl.style.animationName = "bird-light";
        }

        return (
            <div className="boost" onClick={this.handleRule.bind(this)}>
                <div className={styles} onAnimationEnd={this.handleRewardAnimEnd.bind(this)}>
                    <div className="bird">
                    </div>
                    <div className="wrap">
                        <div className="bar">
                            <div className="progress-bar" style={{ width: (width >= 100 ? "100%" : width + "%") }}>
                            </div>
                            <div className="progress-light"
                                ref={el => this.playLightEl = el}
                                style={{ left: (width >= 100 ? "100%" : width + "%") }}
                                onAnimationEnd={this.handleLightAnimEnd.bind(this)}
                            >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

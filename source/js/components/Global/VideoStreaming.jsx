import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import streamClientSDK from 'stream-client-sdk';
import { videoPlaying, getHostProfile, sendEvent2Game } from 'actions/app';
import Sys from "helpers/deviceDetect";
import { getMultiLangText } from "../../helpers/multiLang"

@connect(state => ({
    isPlayingVideo: state.app.get('isPlayingVideo'),
    accountInfo: state.app.get("accountInfo"),
    settings: state.app.get('settings'),
    isStartGame: state.app.get("isStartGame"),
    isMuteMusic: state.app.get("isMuteMusic"),
    isMuteSounds: state.app.get("isMuteSounds"),
    isMuteGameMusic: state.app.get("isMuteGameMusic"),
    musicList: state.app.get("musicList"),
}))

export default class VideoStreaming extends Component {
    constructor(props) {
        super(props);
        this.isConnected = false;
        this.isFocus = true;
        this.state = {
            random: 0,
            currMusicIndex: 0,
            isAutoPlay: false,
            backGame: false,
        }
    }

    componentWillReceiveProps(nextProps) {
        this.installVideoPlayer(nextProps);
    }

    /**
     * 暫停所有音樂音效以及live streaming
     */
    handleBlurEvent() {
        const { isMuteGameMusic, isMuteSounds } = this.props;

        this.handleVideoStreamingPlay("pause");
        this.handleBGMPlay("pause");
        this.setState({ "isAutoPlay": false });
        sendEvent2Game("notify:SoundManager.isFocus", [{
            isFocus: false,
            isMuteSounds,
            isMuteGameMusic,
        }]);
    }

    /**
     * 接續著播放
     */
    handleFocusEvent() {
        const { isStartGame } = this.props;
        const { backGame } = this.state;

        if (!isStartGame || backGame) {
            return;
        }

        this.setState({ backGame: true });
    }

    /**
     * back game button click event
     */
    handleBackGameEvent() {
        const { isPlayingVideo, isMuteMusic, isStartGame, isMuteGameMusic, isMuteSounds } = this.props;

        if (isPlayingVideo) {
            this.handleVideoStreamingPlay("play");
        }

        // 音樂非靜音&&沒有主播&& start page 進入之後 則需要播放 live 背景音樂
        if (!isMuteMusic && !isPlayingVideo && isStartGame) {
            this.handleBGMPlay("play");
            this.setState({ "isAutoPlay": true });
        }
        sendEvent2Game("notify:SoundManager.isFocus", [{
            isFocus: true,
            isMuteSounds,
            isMuteGameMusic,
        }]);

        this.setState({ backGame: false });
    }

    componentDidMount() {
        this.installVideoPlayer(this.props);

        // desktop 沒有focus也要持續播放
        if (Sys.isDesktop) {
            return;
        }

        const self = this;

        let hidden = "hidden";

        if (hidden in document) {
            document.addEventListener("visibilitychange", onchange);
        }
        else if ((hidden = "mozHidden") in document) {
            document.addEventListener("mozvisibilitychange", onchange);
        }
        else if ((hidden = "webkitHidden") in document) {
            document.addEventListener("webkitvisibilitychange", onchange);
        }
        else if ((hidden = "msHidden") in document) {
            document.addEventListener("msvisibilitychange", onchange);
        }
        // IE 9 and lower:
        else if ("onfocusin" in document)
            document.onfocusin = document.onfocusout = onchange;
        // All others:
        else {
            window.onpageshow = window.onpagehide
                = window.onfocus = window.onblur = onchange;
        }

        function onchange(evt) {
            const v = "visible";
            const h = "hidden";
            const evtMap = {
                focus: v,
                focusin: v,
                pageshow: v,
                blur: h,
                focusout: h,
                pagehide: h
            };

            evt = evt || window.event;
            if (evt.type in evtMap) {
                if (evtMap[evt.type] === "visible") {
                    self.handleFocusEvent();
                }
                else {
                    self.handleBlurEvent();
                }
            }
            else {
                if (document[hidden]) {
                    self.handleBlurEvent();
                }
                else {
                    self.handleFocusEvent();
                }
            }
        }

        // set the initial state (but only if browser supports the Page Visibility API)
        if (document[hidden] !== undefined) {
            onchange({ type: document[hidden] ? "blur" : "focus" });
        }

    }

    /**
     * 影音串流play控制
     */
    handleVideoStreamingPlay = (type) => {
        console.log("handleVideoStreamingPlay", type)
        switch (type) {
            case "pause":
                this.isFocus = false;
                streamClientSDK.pauseVideo();
                break;
            case "play":
                this.isFocus = true;
                streamClientSDK.playVideo();
                // 有主播播放時，要把live背景音樂關掉
                this.handleBGMPlay("pause");
                this.setState({ "isAutoPlay": false });
                break;
        }
    }

    /**
     * live 背景音樂控制
     */
    handleBGMPlay = (type) => {
        switch (type) {
            case "pause":
                this.funLive_BGM.pause();
                break;
            case "play":
                this.funLive_BGM.muted = false;
                this.funLive_BGM.play();
                break;
        }
    }

    /**
     * live背景音樂播放完畢事件
     * 會自動撥播放下一首
     */
    handleBGMEnded = () => {
        const { musicList } = this.props;
        const { currMusicIndex } = this.state;

        this.setState({
            currMusicIndex: currMusicIndex + 1 >= musicList.get("list").size ? 0 : currMusicIndex + 1,
        });
    }

    /**
     * 主播不再時，要顯示 fun live layout 且要播放 live 背景音樂
     */
    renderFunLive = () => {
        const { isPlayingVideo, isStartGame, isMuteMusic, musicList } = this.props;
        const { list = [], url } = musicList.toJSON();
        const { isAutoPlay, currMusicIndex } = this.state;
        const style = classNames({
            "fun-live": true,
            'active': !isPlayingVideo,
        });

        // start page進入後才能開始播放
        if (this.funLive_BGM && isStartGame) {
            // 主播是否在線
            if (!isPlayingVideo) {
                if (this.funLive_BGM.paused && !isMuteMusic && this.isFocus) {
                    this.handleBGMPlay("play");
                }
                else if (isMuteMusic) {
                    this.handleBGMPlay("pause");
                }
            }
            else if (!this.funLive_BGM.paused) {
                this.handleBGMPlay("pause");
            }
        }

        return (
            <div className={style}>
                <audio ref={el => this.funLive_BGM = el}
                    src={url + list[currMusicIndex]}
                    muted
                    onEnded={this.handleBGMEnded}
                    autoPlay={isAutoPlay}
                />
            </div>
        )
    }

    /**
     * 顯示不支援瀏覽器的layout
     */
    renderNotSupport = () => {
        return (
            <div className="not-support">
                <div className="mask"></div>
                <div className="content">
                    <div className="icon">
                        <i className="safari"></i>
                        <i className="chrome"></i>
                    </div>
                    <div className="txt">{getMultiLangText("Err_Browser_01")}</div>
                </div>
            </div>
        )
    }

    /**
     * 初始化 vedio streaming
    */
    installVideoPlayer = props => {
        const { settings, dispatch, accountInfo } = props;
        const { productId = "Live_FireDiceTurbo" } = accountInfo ? accountInfo.toJSON() : {};
        const { wsHost, wsPort, wsProtocol, videoBasePath } = settings ? settings.toJS() : {};

        if (!this.isConnected && wsHost && wsPort && videoBasePath && Sys.isSupport) {
            this.isConnected = true;
            streamClientSDK.init({
                rootEl: this.player,
                host: wsHost,
                port: wsPort,
                protocol: wsProtocol,
                productId,
                videoBasePath,
                muted: this.state.random === 0,
                onPlaying: e => {
                    console.log("playing", e);
                    streamClientSDK.removeClass("leave");
                    dispatch(videoPlaying(true));
                },
                onEnd: e => {
                    console.log("ends", e);
                    dispatch(videoPlaying(false));
                    streamClientSDK.addClass("leave");
                },
                onStart: (e) => {
                    dispatch(getHostProfile(e.host));
                    console.log("start", e);
                },
                onError: (e) => {
                    console.log("streaming error");
                    this.isConnected = false;
                    this.installVideoPlayer(this.props);
                    console.error(e);
                }
            }).then(socket => {
                console.log('socket', socket);
                this.setState({ random: this.state.random + 1 });
            });
        }
    }

    /**
     * render back game layout for mobile
     */
    renderBackGame() {
        if (Sys.isDesktop) {
            return null;
        }

        const { isStartGame } = this.props;
        const { backGame } = this.state;

        if (!isStartGame || !backGame) {
            return null;
        }

        return (
            <div className="back-game" onClick={this.handleBackGameEvent.bind(this)}>
                <div className="mask"></div>
                <a className="start-btn"></a>
            </div>
        )
    }

    render() {
        const style = classNames({
            "ucBrowser": Sys.isMobile && Sys.isUCBrowser,
        });

        return (
            <div>
                {this.renderFunLive()}
                <video ref={el => this.player = el} className={style}></video>
                {Sys.isSupport ? null : this.renderNotSupport()}
                {this.renderBackGame()}
            </div>
        )
    }
}

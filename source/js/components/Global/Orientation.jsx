import React, { Component } from 'react';
import { getMultiLangText } from "../../helpers/multiLang";

export default class Orientation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            oriState: "",
        }
    }

    componentDidMount() {
        window.addEventListener("orientationchange", this.handleOrientation);
        this.handleOrientation();
    }

    handleOrientation = () => {
        setTimeout(() => {
            if (window.innerHeight < window.innerWidth) {
                this.setState({ oriState: "landscape" });
            } else {
                if (window.innerHeight < window.innerWidth) {
                    this.setState({ oriState: "landscape" });
                } else {
                    this.setState({ oriState: "portrait" });
                }
            }
        }, 300)
    }

    render() {
        const { oriState } = this.state;

        if (oriState === "landscape") {
            return (
                <div className="ori-note">
                    <div className="mask"></div>
                    <div className="content">
                        <div className="icon"></div>
                        <div className="txt">{getMultiLangText("Web_Live_000001")}</div>
                    </div>
                </div>
            )
        }
        else {
            return null;
        }
    }
}

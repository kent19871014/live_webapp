import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { startGame, openSounds } from "actions/app";
import { connect } from 'react-redux';
import streamClientSDK from 'stream-client-sdk';
import NoSleep from "nosleep.js";
import Sys from "helpers/deviceDetect";
import { getMultiLangText } from "../../helpers/multiLang";

@connect(state => ({
    isPlayingVideo: state.app.get('isPlayingVideo'),
    accountInfo: state.app.get('accountInfo'),
    isMuteSounds: state.app.get("isMuteSounds"),
    isMuteGameMusic: state.app.get("isMuteGameMusic"),
}))

export default class StartPage extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.dispatch(openSounds(true));
        if (Sys.isMobile) {
            this.noSleep = new NoSleep();
            document.addEventListener('touchstart', this.enableNoSleep, false);
            if (Sys.isAndroidDevice) {
                document.addEventListener("backbutton", () => {

                }, false);
            }
        }
    }

    enableNoSleep = () => {
        this.noSleep.enable();
        document.removeEventListener('touchstart', this.enableNoSleep, false);
    }

    onEmptyZone = (e) => {

    }

    onStartBtnClick = (e) => {
        const { dispatch, isMuteSounds, isMuteGameMusic } = this.props;

        streamClientSDK.toggleMute(false);
        this.props.dispatch(openSounds(false));
        dispatch(startGame({ isMuteSounds: false, isMuteGameMusic }));
    }

    renderCopyright() {
        const { accountInfo } = this.props;
        const { productId } = accountInfo.toJSON();
        if (productId.split("_")[1] === "PB") {
            return (
                <div className="copyright">
                    © 2020 Playboy Enterprises, Inc. All Rights Reserved
                </div>
            )
        }
        else {
            return null
        }
    }

    render() {
        const { accountInfo } = this.props;
        const { lang, productId } = accountInfo ? accountInfo.toJSON() : {};


        return (
            <div className={`start-page ${productId}`} onClick={this.onEmptyZone}>
                <div className="wrapper">
                    <div className="game-logo" lang={lang}>
                    </div>
                    <a className="start-btn" onClick={this.onStartBtnClick}></a>
                    {
                        !Sys.isGoodExperience ?
                            <div className="txt">{getMultiLangText("Err_Browser_02")}</div>
                            : null
                    }

                </div>
                {this.renderCopyright()}
            </div>
        )
    }
}

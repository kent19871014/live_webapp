import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { selectGift, unselectGift } from 'actions/app';

@connect(state => ({
    accountInfo: state.app.get('accountInfo'),
    selectedGiftId: state.gift.get('selectedGiftId'),
}))
export default class Gift extends Component {
    static propTypes = {
        selectedGiftId: PropTypes.string,
        dispatch: PropTypes.func,
        giftInfo: PropTypes.object,
        giftType: PropTypes.number
    }

    static defaultProps = {
        giftInfo: {},
    }

    handleGiftClick = e => {
        const { dispatch } = this.props;
        const el = e.currentTarget;

        if (el.checked) {
            dispatch(selectGift(el.value));
        }
        else {
            dispatch(unselectGift());
        }
    }

    render() {
        const { selectedGiftId, giftInfo, accountInfo, giftType } = this.props;
        if(!giftInfo) {
            return (
                <label className="gift coming-soon">
                    <input type="checkbox" name="gift" />
                    <img className='gift-image' src="./assets/img/icon/coming-soon.svg" />
                    <span className='gift-value coming-soon'>coming soon</span>
                </label>
            );
        }
        const { giftId, values, iconUrl = '', count } = giftInfo;
        const { currency } = accountInfo ? accountInfo.toJSON() : {}
        const price = values[currency] ? values[currency] : "";
        const selected = giftId === selectedGiftId;
        const hasNoPrice = !price;
        const style = classNames({
            gift: true,
            selected,
        });

        return (
            <label className={ style }>
                <input
                    type="checkbox"
                    name="gift"
                    checked={ selected }
                    defaultValue={ giftId }
                    disabled={ hasNoPrice }
                    onChange={ this.handleGiftClick }
                />
                <img className='gift-image' src={ iconUrl } />
                <span className='gift-value'>{ giftType === 0 ? price : "X" + count }</span>
            </label>
        );
    }
}

import React, { Component } from 'react';
import classNames from 'classnames';

export default class MenuBarItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isPlay: false,
        }

    }

    onAnimEnd = e => {
        const { handleAnimEnd } = this.props;
        this.setState({
            isPlay: false,
        })

        if (handleAnimEnd) {
            handleAnimEnd();
        }
    }


    onClickEvent = e => {
        this.setState({
            isPlay: true,
        })

        const { handleClick } = this.props;

        if (handleClick) {
            handleClick();
        }
    }

    render() {
        const { icon, clickAnimName, style, classStyle = {}, disabled = false, active = false } = this.props;
        const { isPlay } = this.state;
        const className = {
            "disabled": disabled,
            "active": active,
            ...classStyle
        };

        className[`btn icon-${icon}`] = true;

        if(clickAnimName) {
            className[clickAnimName] = isPlay;
        }

        return (
            <span className={classNames(className)}
                onClick={(disabled || active) ? null : this.onClickEvent}
                onAnimationEnd={this.onAnimEnd}
                style={style}>
            </span>
        )
    }
}

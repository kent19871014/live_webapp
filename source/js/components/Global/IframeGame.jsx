import React, { Component } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import {
    sendEvent2Game,
} from "actions/app";

@connect(state => ({
    settings: state.app.get('settings'),
    isLoading: state.app.get('isLoading'),
    accountInfo: state.app.get("accountInfo"),
    screenSize: state.app.get("screenSize"),
    isReconnect: state.app.get("isReconnect"),
}))

export default class IframeGame extends Component {

    constructor(props) {
        super(props);

        this.state = {
            randomIframe: 0,
        }
        this.reconnectLoop = null;
    }

    handleIframeLoaded = () => {
        const { dispatch } = this.props;

        if (this.gameClient) {
            if (this.gameClient.contentDocument) {
                if (this.gameClient.contentDocument.title.indexOf("404") >= 0) {
                    this.resetIframe();
                }
            }
        }
    }

    resetIframe() {
        this.setState({ randomIframe: this.state.randomIframe + 1 });
    }

    reconnectTimer = () => {
        if (this.reconnectLoop != null) {
            return;
        }

        this.resetIframe();
        this.reconnectLoop = window.setInterval(() => {
            this.resetIframe();
        }, 15000)
    }

    clearReconnectTimer = () => {
        if (this.reconnectLoop >= 0) {
            window.clearInterval(this.reconnectLoop);
            this.reconnectLoop = null;
        }
    }


    render() {
        const { isLoading, isReconnect, accountInfo, settings, screenSize } = this.props;
        const { gameClientUrl, gameSize } = settings ? settings.toJSON() : {};
        // const { innerWidth, innerHeight } = this.state;
        const { account, lang, currency, token, gameName, licensee, productId } = accountInfo.toJSON();

        let src = gameClientUrl.replace("{gameID}", productId);

        src += `?Account=${account || ""}`;
        src += `&lang=${lang || "zh-cn"}`;
        src += `&currency=${currency || "CNY"}`;
        src += `&gamename=${gameName || "Live_FireDiceTurbo"}`;
        src += `&licensee=${licensee || "mg"}`;
        src += `&token=${token || ""}`;
        src += `&randomIframe=${this.state.randomIframe}`;

        if (this.gameClient) {
            this.gameClient.style.height = (screenSize.toJSON().screenWidth / (gameSize.w / gameSize.h)) + "px";
        }

        if (isReconnect) {
            this.reconnectTimer();
        }
        else {
            this.clearReconnectTimer();
        }

        return (
            <iframe
                id="iframe-game"
                key={this.state.IframeGame}
                style={{ display: (isReconnect ? "none" : "") }}
                name="game"
                ref={el => this.gameClient = el}
                src={src}
                onLoad={this.handleIframeLoaded}
            />
        );
    }
}

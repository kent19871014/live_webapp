import React, { Component } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { removeGreeting } from "actions/app";

@connect(state => ({
    greeting: state.gift.get('greeting'),
}))

export default class MenuBar extends Component {
    constructor(props) {
        super(props);
    }

    handleGiftAnimationEnd (event) {
        const { dispatch, greeting } = this.props;
        const item = greeting.filter(item => item.key === event.currentTarget.dataset.key).get(0);
        dispatch(removeGreeting(greeting.indexOf(item)));
    }

    render() {
        const { greeting } = this.props;
        // let src = './assets/img/icon/star.png';
        // src = './assets/img/icon/heart.png';
        return (
            <div className='greetings'>
            {
                greeting.map(({ src, key, style }, i) => {
                    return (
                        <img
                            key={key}
                            data-key={key}
                            src={src}
                            className={classNames(style)}
                            onAnimationEnd={this.handleGiftAnimationEnd.bind(this)}
                        />
                    );
                })
            }
            </div>
        )
    }
}

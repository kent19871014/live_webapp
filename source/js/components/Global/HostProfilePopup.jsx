import React, { Component } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { openHostProfile, sendGift } from 'actions/app'

@connect(state => ({
    isOpenHostProfile: state.host.get('isOpenHostProfile'),
    hostAccount: state.host.get('hostAccount'),
    hostProfile: state.host.get('hostProfile'),
    accountInfo: state.app.get('accountInfo'),
    settings: state.app.get('settings'),
    freeGiftList: state.gift.get('freeGiftList'),
    allGiftList: state.gift.get("allGiftList"),
    hostGiftAmount: state.gift.get("hostGiftAmount"),
}))

export default class HostProfilePopup extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isPlayHeartBeatAnim: false,
        }

    }

    handleProfile = () => {
        const { dispatch } = this.props;
        dispatch(openHostProfile(false));
    }

    countGiftAmount = (tag) => {
        const { allGiftList, hostGiftAmount } = this.props;
        const regex = new RegExp(tag, "g");

        let amount = 0;
        allGiftList.filter(item => item.get("tags").join(",").match(regex)).map((allGift) => {
            hostGiftAmount.filter(item => item.get("giftId") === allGift.get("giftId")).map((hostGift) => {
                amount += hostGift.get("giftAmount");
            })
        })

        return amount;
    }

    onHeartAnimEnd = e => {
        this.setState({
            isPlayHeartBeatAnim: false
        })
    }

    handleGreeting = () => {
        const { dispatch, hostAccount, freeGiftList } = this.props;
        const { sub, token: hostToken } = hostAccount ? hostAccount.toJSON() : {};
        // random free gift
        const randomList = (freeGiftList ? freeGiftList.toJS() : [])
            .sort((a, b) => {
                return Math.random() - 0.5;
            });

        if (randomList.length === 0) {
            console.error("no free gift");
            return;
        }

        const gift = randomList[0];
        dispatch(sendGift({
            giftId: gift.giftId,
            receiverId: sub || hostToken,
        }));
        this.setState({
            isPlayHeartBeatAnim: true
        })
    }

    checkNewLine(txt) {
        return txt.split("\\n").map((item) => {
            return (
                <p>{item}</p>
            )
        })
    }

    render() {
        const { hostProfile, isOpenHostProfile, accountInfo, settings } = this.props;
        const {
            productId, lang
        } = accountInfo.toJSON();
        const { def } = settings.get("lang").toJSON();
        const { imgUrl, content } = hostProfile.toJSON();
        const { name, detail = [], talks = "" } = content[lang] || (content[def] || {});
        const { isPlayHeartBeatAnim } = this.state;

        const style = classNames({
            "host-profile-popup": true,
            "active": isOpenHostProfile,
        });

        const heartStyle = classNames({
            "like-btn": true,
            "pulse": isPlayHeartBeatAnim,
        })

        const { profile } = imgUrl;

        return (
            <div className={style}>
                <div className="mask"></div>
                <div className="profile">
                    <div className="pic" style={{ "backgroundImage": `url(${profile[productId] ? profile[productId] : profile.def})` }}>
                        <div className="gradient"></div>
                    </div>
                    <div className="content">
                        <div className="name">
                            <p className="txt">{name}</p>
                            <p className={heartStyle} onClick={this.handleGreeting} onAnimationEnd={this.onHeartAnimEnd}>
                                <i className="icon-heart-full"></i>
                            </p>
                        </div>
                        <div className="like">
                            <i className="icon"></i>
                            <p className="number">{this.countGiftAmount("free")}</p>
                        </div>
                        <div className="content-wrapper scrollbar-style">
                            <div className="detail">
                                {detail.map((item) => {
                                    return (
                                        <div className="item">
                                            <i className={item.icon + " icon"}></i>
                                            <span className="title">{item.title}</span>
                                            <span className="txt">{this.checkNewLine(item.txt)}</span>
                                        </div>
                                    )
                                })}
                            </div>
                            <div className="words" >
                                {this.checkNewLine(talks)}
                            </div>
                        </div>
                    </div>
                    <div className="btn" onClick={this.handleProfile}>
                        <i className="icon-close">
                        </i>
                    </div>
                </div>
            </div>
        );
    }
}

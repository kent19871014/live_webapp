import React, { Component } from 'react';

export default class LoadingPage extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <div className="loading-page">
                <div className="mg">
                    <span className="mg-logo"></span>
                </div>
                <div className="loading">Loading...</div>
            </div>
        )
    }
}

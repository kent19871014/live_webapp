import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getMultiLangText } from "../../helpers/multiLang";
import { sysErrorMsg } from 'actions/app';

@connect(state => ({
    sysErrorMsg: state.app.get("sysErrorMsg"),
}))

export default class SysErrorMessage extends Component {
    constructor(props) {
        super(props);
    }

    hadnleReload = () => {
        // location.reload();
        const { dispatch } = this.props;
        dispatch(sysErrorMsg({
            txt: "",
            errcode: -1
        }));
    }

    render() {
        const { sysErrorMsg } = this.props;
        const { txt, errcode } = sysErrorMsg.toJSON();
        if (!txt) {
            return null;
        }
        return (
            <div className="sys-error-msg">
                <div className="mask" onClick={this.hadnleReload}></div>
                <div className="content" onClick={this.hadnleReload}>
                    <div className="msg">{txt}</div>
                    <div className="msg">{errcode}</div>
                    <div className="button"><i className="icon"></i></div>
                </div>
            </div>
        )
    }
}

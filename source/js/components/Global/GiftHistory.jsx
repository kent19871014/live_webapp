import React, { Component } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux';
import {
    OPEN_GIFT_HISTORY,
    OPEN_SUB_MENU_BAR,
} from 'actions/type';
import { openUI } from 'actions/app';

@connect(state => ({
    settings: state.app.get('settings'),
    accountInfo: state.app.get("accountInfo"),
    isOpenGiftHistory: state.app.get("isOpenGiftHistory"),
}))

export default class GiftHistory extends Component {
    constructor(props) {
        super(props);
        this.src = "";
    }

    componentWillReceiveProps(props) {
        const { settings, accountInfo } = props;
        const { historyClientUrl } = settings ? settings.toJSON() : {};
        const { account, token, lang, currency, licensee, productId } = accountInfo ? accountInfo.toJSON() : {};
        this.src = `${historyClientUrl}?account=${account || ""}&token=${token || ""}&lang=${lang || "zh-cn"}&currency=${currency || "CNY"}&licensee=${licensee || "mg"}&gameName=${productId || "Live_FireDiceTurbo"}`
    }

    handleCloseHistory = () => {
        const { dispatch } = this.props;
        dispatch(openUI(OPEN_GIFT_HISTORY, false));
        dispatch(openUI(OPEN_SUB_MENU_BAR, false));
    }

    render() {
        const { isOpenGiftHistory } = this.props;

        const inlineStyle = classNames({
            "scroll-wrapper": true,
            "gift-history": true,
            active: isOpenGiftHistory
        });

        return (
            <div className={inlineStyle}>
                <div className="mask" onClick={this.handleCloseHistory}></div>
                <div className="wrapper">
                    <div className="bar"></div>
                    <iframe src={isOpenGiftHistory ? this.src : ""}>
                    </iframe>
                </div>
            </div>
        );
    }
}

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getMultiLangText } from "../../helpers/multiLang";
import {
    sendEvent2Game,
} from "actions/app";

@connect(state => ({
    isReconnect: state.app.get("isReconnect"),
}))

export default class IframeReconnectGame extends Component {

    constructor(props) {
        super(props);
        this.reconnectLoop = null;
    }

    render() {
        const { isReconnect } = this.props;

        return(
            <div className="iframe-reconnect-game" style={{ display: (isReconnect ? "" : "none") }}>
                <div className="mask"></div>
                <div className="content">
                <div className="msg">{getMultiLangText("Err_Reconnect")}</div>
                    <div className="loading"></div>
                </div>
            </div >
        );
    }
}

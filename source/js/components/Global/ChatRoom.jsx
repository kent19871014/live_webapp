import React, { Component } from 'react';
import { connect } from 'react-redux';
import MessageItem from './MessageItem';
import { connectChatServer, saveNickname, userErrorMsg, isConnectedChatServer, joinGroupChatRoom } from 'actions/app';
import { getMultiLangText } from "../../helpers/multiLang"
import chatroomError from "../../error/chatroomError";

@connect(state => ({
    settings: state.app.get('settings'),
    accountInfo: state.app.get('accountInfo'),
    screenSize: state.app.get("screenSize"),
    msgList: state.chat.get('msgList'),
    isConnected: state.chat.get("isConnected"),
    isSaveNickname: state.chat.get("isSaveNickname"),
    isJoinGroupChat: state.chat.get("isJoinGroupChat"),
}))

export default class ChatRoom extends Component {
    constructor(props) {
        super(props);
        this.isNeed2Bottom = true;
        this.clearWarningTimer = null;
        this.state = {
            isShowWarning: true,
        }
    }

    componentDidUpdate() {
        if (this.isNeed2Bottom) {
            this.handleScroll2End();
        }
    }

    componentDidMount() {
        const { dispatch, settings, accountInfo } = this.props;
        const { chatRoom } = settings ? settings.toJSON() : {};
        const { forFunChatName } = chatRoom;
        const { token } = accountInfo ? accountInfo.toJSON() : {};
        const connect = () => {
            connectChatServer({
                reconnectFun,
            }).then((receive) => {
                dispatch(isConnectedChatServer(true));
                receive();
                if(!token) {
                    dispatch(saveNickname(forFunChatName, false));
                }
            })
            .catch((data) => {
                dispatch(userErrorMsg(chatroomError(data)))
            });
        }

        const reconnectFun = () => {
            dispatch(isConnectedChatServer(false));
            dispatch(userErrorMsg({
                errcode: undefined,
                txt: getMultiLangText("Err_ChatReconnect"),
            }))
            setTimeout(connect, 15000, 0);
        }

        this.handleScroll2End();

        connect();

    }

    handleUserScroll = () => {
        const { chatRoomElem } = this.refs;
        const { scrollTop, scrollHeight, offsetHeight, clientHeight } = chatRoomElem;

        this.isNeed2Bottom = scrollHeight - scrollTop - clientHeight < 10;
    }

    handleScroll2End = () => {
        const { chatRoomElem } = this.refs;
        const { scrollHeight, offsetHeight } = chatRoomElem;
        chatRoomElem.scrollTop = scrollHeight;
    }

    renderChatRoomWarning = () => {
        const { chatRoomElem } = this.refs;
        const { isJoinGroupChat } = this.props;
        const { isShowWarning } = this.state;

        if(isJoinGroupChat && isShowWarning) {
            if(!this.clearWarningTimer) {
                this.clearWarningTimer = setTimeout(() => {
                    this.setState({"isShowWarning": false});
                }, 8000);
            }

            return (
                <div className="msg chat-system" style={{width: (chatRoomElem ? chatRoomElem.clientWidth : 0)}}>
                    <span className='user-name'>{getMultiLangText("Txt_Hint_01")}</span>
                    <span className='user-text'>{getMultiLangText("Txt_Hint_02")}</span>
                </div>
            )
        }
        else {
            return null;
        }

    }

    renderWelcomMsg = ({ externalId, token }) => {
        if (token) {
            return (
                <div className="msg chat-owner">
                    <span className='user-name'>{externalId + ': '}</span>
                    <span className='user-text'>{getMultiLangText("RULE_023")}</span>
                </div>
            )
        }
        else {
            return null
        }
    }

    render() {
        const { dispatch, msgList, screenSize, settings, isJoinGroupChat, isConnected, isSaveNickname } = this.props;
        const { gameSize } = settings ? settings.toJSON() : {};
        const bottom = (screenSize.toJSON().screenWidth / (gameSize.w / (gameSize.h - gameSize.offsetH)) + 52) + "px";

        if(!isJoinGroupChat && isConnected && isSaveNickname) {
            dispatch(joinGroupChatRoom());
        }
        return (
            <div className="chat-room scrollbar-style" ref="chatRoomElem" style={{ bottom }} onScroll={this.handleUserScroll}>
                {/* {this.renderChatRoomWarning()} */}
                {/* {this.renderWelcomMsg({ externalId, token })} */}
                {msgList.map((item, idx) => {
                    return (<MessageItem
                        key={`msg-${idx}`}
                        player={item.player}
                        content={item.content}
                        isOwner={item.isOwner}
                    />);
                })}
            </div>
        );
    }
}

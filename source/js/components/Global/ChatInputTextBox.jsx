import React, { Component } from 'react';
import { connect } from 'react-redux';
import { sendGroupChatMsg, openUI, userErrorMsg } from 'actions/app';
import emoji from 'emoji-regex';
import { OPEN_CHAT_INPUT_BOX } from 'actions/type';
import { getMultiLangText } from "../../helpers/multiLang";
import { cleanBadWords } from "../../helpers/badWords";

@connect(state => ({
    isOpenChatInputText: state.app.get("isOpenChatInputText"),
}))
export default class ChatInputTextBox extends Component {
    constructor(props) {
        super(props);
        this.inputVal = "";
        this.lastSpeakTime = new Date().getTime();
        this.lastSpeakInterval = 999999;
        this.isOnComposition = false;
        this.isSendBtnPressed = false;
    }

    handlePaste(event) {
        event.preventDefault();
        return false;
    }

    handleMsgSend(event) {
        const { dispatch, isOpenChatInputText } = this.props;
        const currSpeakTime = new Date().getTime();
        const currInterval = currSpeakTime - this.lastSpeakTime;

        if (this.inputVal === "") {
            return false;
        }
        // url
        if (this.inputVal.match(/(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+/g)) {
            dispatch(userErrorMsg({
                txt: getMultiLangText("Err_ChatMsg_01")
            }));
        }
        // mail
        else if (this.inputVal.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)) {
            dispatch(userErrorMsg({
                txt: getMultiLangText("Err_ChatMsg_01")
            }));
        }
        else if (this.lastSpeakInterval <= 1000 && currInterval <= 3000) {
            dispatch(userErrorMsg({
                txt: getMultiLangText("Err_ChatMsg_02")
            }));
        }
        else {
            this.lastSpeakTime = currSpeakTime;
            this.lastSpeakInterval = currInterval;
            dispatch(sendGroupChatMsg(cleanBadWords(this.inputVal))).then(() => {
                this.inputVal = "";
            });
            dispatch(openUI(OPEN_CHAT_INPUT_BOX, !isOpenChatInputText));
            this.textInput.blur();
        }
    }


    handleComposition(event) {
        if (event.type === 'compositionend') {
            this.inputVal = event.target.value;
            // composition is end
            this.isOnComposition = false;
        }
        else {
            // in composition
            this.isOnComposition = true;
        }
    }

    handleInputChange(event) {
        // only when onComposition===false to fire onChange
        if (event.target instanceof HTMLInputElement && !this.isOnComposition) {
            event.target.value = event.target.value.replace(emoji(), '');
            this.inputVal = event.target.value;
        }
    }

    handleInputBlur = () => {
        const { dispatch, isOpenChatInputText } = this.props;

        dispatch(openUI(OPEN_CHAT_INPUT_BOX, !isOpenChatInputText));
    }

    handleUserInputEvent(event) {
        switch (event.type) {
            case "mousedown":
            case "touchstart":
                this.isSendBtnPressed = true;
                break;
            case "mouseup":
            case "touchend":
                this.isSendBtnPressed = false;
                break;
            default:
                break;
        }
    }

    handleKeyEvent(event) {
        if (event.keyCode === 13) {
            this.handleMsgSend();
        }
    }

    componentDidUpdate() {
        const { isOpenChatInputText } = this.props;

        if (isOpenChatInputText && this.textInput) {
            this.textInput.focus();
        }
    }

    render() {
        const { isOpenChatInputText } = this.props;

        if (!isOpenChatInputText && this.textInput) {
            this.textInput.value = "";
        }

        return (
            <div className='input-text-box' style={{ display: (isOpenChatInputText ? "" : "none") }}>
                <div className="mask" onClick={this.handleInputBlur} ></div>
                <section className='input-section'>
                    <input type="text" className='box' ref={input => this.textInput = input}
                        maxLength="50"
                        placeholder={getMultiLangText("Txt_Hint_03")}
                        onCompositionStart={this.handleComposition.bind(this)}
                        onCompositionUpdate={this.handleComposition.bind(this)}
                        onCompositionEnd={this.handleComposition.bind(this)}
                        // onBlur={this.handleInputBlur.bind(this)}
                        onChange={this.handleInputChange.bind(this)}
                        onKeyUp={this.handleKeyEvent.bind(this)}
                    />
                    <button className='btn btn-send'
                        onClick={this.handleMsgSend.bind(this)}
                        onMouseDown={this.handleUserInputEvent.bind(this)}
                        onMouseUp={this.handleUserInputEvent.bind(this)}
                        onTouchStart={this.handleUserInputEvent.bind(this)}
                        onTouchEnd={this.handleUserInputEvent.bind(this)}
                    >
                        <img src='./assets/img/icon/send.svg' />
                    </button>
                </section>
            </div>
        );
    }
}

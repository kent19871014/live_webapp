import React, { Component } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { displayBoostRule } from 'actions/app';
import { openUI, setCurrentGiftTab } from 'actions/app';
import { getMultiLangText } from "../../helpers/multiLang";
import { OPEN_GIFT_LIST } from 'actions/type';

@connect(state => ({
    accountInfo: state.app.get('accountInfo'),
    settings: state.app.get('settings'),
    currencyConfig: state.app.get("currencyConfig"),
    isOpenRule: state.boost.get("isOpenRule"),
    targetPower: state.boost.get("targetPower"),
    isFirst: state.boost.get("isFirst"),
}))

export default class BoostRule extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isCloseing: false,
        }
    }

    handleHideRule = () => {
        this.setState({
            isCloseing: true,
        })
    }

    handleCloseAnim = () => {
        const { dispatch, isOpenRule } = this.props;
        const { isCloseing } = this.state;

        if (isOpenRule && !isCloseing) {
            return;
        }

        this.setState({
            isCloseing: false,
        })
        dispatch(displayBoostRule(false));


    }

    openBackpack() {
        const { dispatch } = this.props;

        this.handleHideRule();
        dispatch(setCurrentGiftTab(1));
        dispatch(openUI(OPEN_GIFT_LIST, true))
    }

    getTargetPower = (targetPower) => {
        const { accountInfo, currencyConfig } = this.props;
        const { currency } = accountInfo.toJSON();
        const config = currencyConfig.toJSON();
        const { devaluationFactor } = config[currency.toUpperCase()];

        if (devaluationFactor) {
            return targetPower / devaluationFactor;
        }
        else {
            return targetPower;
        }
    }

    render() {
        return null;

        const { isOpenRule, settings, targetPower, isFirst } = this.props;
        const { boost } = settings.toJSON();
        const { isCloseing } = this.state;
        const style = classNames((isFirst && !isCloseing) ? {
            boostRule: true,
            active: true,
            close: false,
        }:{
            boostRule: true,
            active: !isCloseing && isOpenRule,
            close: isCloseing,
        });

        return (
            <div className={style} onTransitionEnd={this.handleCloseAnim}>
                <div className="mask"></div>
                <div className="wrapper">
                    <div className="btn" onClick={this.handleHideRule}>
                        <span className="icon-close"></span>
                    </div>
                    <div className="header">
                        <span>{getMultiLangText("Rul_Boost_01")}</span>
                    </div>
                    <div className="content">
                        <span className="text">
                            <span>{getMultiLangText("Rul_Boost_02")}</span>
                            <span className="highlight">{" " + this.getTargetPower(targetPower) + " "}</span>
                            <span>{getMultiLangText("Rul_Boost_03")}</span>
                        </span>
                        <span className="text">
                            {getMultiLangText("Rul_Boost_04")}
                        </span>
                        <div className="img" style={{ 'backgroundImage': "url(" + boost.ruleImg + ')' }}></div>
                        <div className="backpack-btn" onClick={this.openBackpack.bind(this)}>
                            <span>{getMultiLangText("Rul_Boost_05")}</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

export default class MessageItem extends Component {
    static propTypes = {
        msg: PropTypes.string,
        who: PropTypes.string,
    }

    render() {
        const { player, content, isOwner } = this.props;
        const style = classNames({
            "chat-owner": isOwner,
            "chat-other": !isOwner,
        });
        return (
            <div className={`msg ${style}`}>
                <span className='user-name'>{player + ': '}</span>
                <span className='user-text'>{content}</span>
            </div>
        );
    }
}

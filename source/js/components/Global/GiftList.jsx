import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classNames from 'classnames';
import Gift from 'components/Global/Gift';
import { fetchGiftList, fetchBackPack, sendGift, sendBackPackGift, unselectGift, openUI, setCurrentGiftTab } from 'actions/app';
import {
    OPEN_GIFT_LIST,
} from 'actions/type';
import { getMultiLangText } from "../../helpers/multiLang";
import Sys from "helpers/deviceDetect";

@connect(state => ({
    settings: state.app.get("settings"),
    accountInfo: state.app.get("accountInfo"),
    hostAccount: state.host.get("hostAccount"),
    selectedGiftId: state.gift.get('selectedGiftId'),
    giftList: state.gift.get('giftList'),
    backpack: state.gift.get('backpack'),
    isOpenGiftList: state.gift.get("isOpenGiftList"),
    isPlayingVideo: state.app.get('isPlayingVideo'),
    currTab: state.gift.get("currTab"),
}))
export default class GiftList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            pageIndex: 0,
            moveStart: 0,
            currPageIndex: 0,
            tabs: [{
                name: "GIFT",
                pageIndex: 0,
                pageSize: 1,

            }, {
                name: "BACKPACK",
                pageIndex: 0,
                pageSize: 1,
            }]
        };
    }

    componentWillReceiveProps(props) {
        const { tabs } = this.state;
        const { giftList, backpack, settings } = props;

        if (!giftList) {
            return;
        }

        const { giftCntPerPage } = settings ? settings.toJSON() : {};
        const list = [giftList.toJSON(), backpack.toJSON()];

        let newTabs = tabs.map((item, index) => {
            item.pageSize = Math.ceil(list[index].length / giftCntPerPage) || 1;
            return item;
        });

        this.setState({ tabs: newTabs });
    }

    componentDidMount() {
        const { dispatch, accountInfo } = this.props;
        const { token } = accountInfo ? accountInfo.toJSON() : {};

        dispatch(fetchGiftList()).then(() => {
            if (token) {
                dispatch(fetchBackPack());
            }
        });
    }

    handelBubbleUp = e => {
        e.stopPropagation();
    }

    /**
     * 發送禮物
     */
    handleSendGift = e => {
        const { dispatch, selectedGiftId, hostAccount, currTab } = this.props;

        if ((!!hostAccount) == false) {
            console.error("直播室沒有開!!");
            return;
        }

        let { sub, token: hostToken } = hostAccount.toJSON();

        switch (currTab) {
            case 0:
                dispatch(sendGift({
                    giftId: selectedGiftId,
                    receiverId: sub || hostToken,
                }));
                break;
            case 1:
                dispatch(sendBackPackGift({
                    giftId: selectedGiftId,
                    receiverId: sub || hostToken,
                }));
                break;
        }

        dispatch(openUI(OPEN_GIFT_LIST, false));
    }

    /**
     * 關閉禮物清單
     */
    handleCloseGiftList = e => {
        const { dispatch } = this.props;

        dispatch(openUI(OPEN_GIFT_LIST, false));
        dispatch(unselectGift());
        return false;
    }

    /**
     * 禮物項目
     */
    handleGiftItem({ tabType, list, giftCntPerPage, start }) {
        const { accountInfo } = this.props;
        const { currency } = accountInfo ? accountInfo.toJSON() : {};

        let item = [];

        for (let j = start * giftCntPerPage; item.length < giftCntPerPage; j++) {
            if (list[j]) {
                const { giftId, values } = list[j];
                const style = classNames({
                    "error": values[currency] === undefined,
                });
                item.push(
                    <li key={`gift-${giftId}`} data-key={`gift-${giftId}`} className={style}>
                        <Gift giftInfo={list[j]} giftType={tabType} />
                    </li>
                )
            }
            else {
                item.push(
                    <li key={`gift-${j}`} data-key={`gift-${j}`}>
                        <Gift giftInfo={null} giftType={tabType} />
                    </li>
                )
            }
        }
        return item;
    }

    /**
     * 禮物分頁swip start event
     */
    handleSwipGiftListStart(event) {
        this.setState({
            "moveStart": (Sys.isMobile ? event.touches[0].clientX : event.clientX)
        });
    }

    /**
     * 禮物分頁swip end event
     */
    handleSwipGiftListEnd(event) {
        const { currTab } = this.props;
        const { tabs, moveStart } = this.state;
        const { pageIndex, pageSize } = tabs[currTab];
        const elem = event.currentTarget;
        const moveEnd = Sys.isMobile ? event.changedTouches[0].clientX : event.clientX;
        const dir = moveStart - moveEnd > 0 ? 1 : -1;
        const newIndex = pageIndex + (1 * dir);

        if (Math.abs(moveStart - moveEnd) > 15) {
            if (newIndex < 0 || newIndex >= pageSize) {
                return;
            }

            let newTabs = tabs;

            newTabs[currTab].pageIndex = newIndex;
            elem.style.transform = `translateX(${-100 * newIndex}%)`
            this.setState({ "tabs": newTabs });
        }
    }

    /**
     * 禮物分頁點點
     */
    handlePagePoint = (renderTab) => {
        const { pageIndex, pageSize } = renderTab;

        let point = [];
        for (let i = 0; i < pageSize; i++) {
            const style = classNames({
                "pagination-point": true,
                "active": pageIndex === i,
            });

            point.push(
                <i className={style} key={`point-${i}`}></i>
            )
        }

        return point;
    }

    /**
     * 禮物 tab 頁簽 gift / backpack
     */
    handleTabs(tabsIndex, event) {
        const { dispatch } = this.props;
        dispatch(setCurrentGiftTab(tabsIndex));
    }

    /**
     * prevent page scrolling on drag
     */
    handleTouchMoveEvent(e) {
        e.preventDefault();
    }

    /**
     * render禮物內容 gift / backpack
     */
    renderGiftContent = ({ list, tabType, giftCntPerPage }) => {
        const { currTab } = this.props;
        const { tabs } = this.state;
        const renderTab = tabs[tabType];
        const { pageIndex } = renderTab;

        return (
            <div className="gift-content-wrapper" style={{ display: tabType === currTab ? "" : "none" }}>
                <div className="gift-content"
                    style={{ transform: `translateX(${-100 * pageIndex}%)` }}
                    onTouchStart={this.handleSwipGiftListStart.bind(this)}
                    onTouchEnd={this.handleSwipGiftListEnd.bind(this)}
                    onMouseDown={this.handleSwipGiftListStart.bind(this)}
                    onMouseUp={this.handleSwipGiftListEnd.bind(this)}
                >
                    {this.renderGift({ list, renderTab, giftCntPerPage, tabType })}
                </div>
                <div className="gift-pagination">{this.handlePagePoint(renderTab)}</div>
            </div>
        )
    }

    /**
     * render gift
     */
    renderGift = ({ list, renderTab, giftCntPerPage, tabType }) => {
        const { pageSize } = renderTab
        let template = [];

        for (let i = 0; i < pageSize; i++) {
            template.push(
                <ul className='gift-list' key={`page-${i}`}>
                    {this.handleGiftItem({ tabType, list, giftCntPerPage, start: i })}
                </ul>
            )
        }

        return template;
    }

    render() {
        const { isOpenGiftList, giftList, backpack, selectedGiftId, isPlayingVideo, accountInfo, settings, currTab } = this.props;
        const { giftCntPerPage } = settings ? settings.toJSON() : {};
        const { token } = accountInfo ? accountInfo.toJSON() : {};
        const { tabs } = this.state;

        return (
            <div className='gift-section' onClick={this.handleCloseGiftList} style={{ display: (!isOpenGiftList || !giftList) ? "none" : "block" }} onTouchMove={this.handleTouchMoveEvent.bind(this)} >
                <div className='gift-list-body' onClick={this.handelBubbleUp} >
                    <ul className="gift-tabs">
                        {tabs.map((item, index) => {
                            const style = classNames({
                                "active": index === currTab,
                            });
                            return <li className={style} key={`tabs-${index}`} onClick={this.handleTabs.bind(this, index)}>{item.name}</li>
                        })}
                    </ul>
                    {this.renderGiftContent({
                        giftCntPerPage,
                        tabType: 0,
                        list: giftList.toJSON()
                    })}
                    {this.renderGiftContent({
                        giftCntPerPage,
                        tabType: 1,
                        list: backpack.toJSON()
                    })}
                    <footer className='footer'>
                        <button
                            className='btn btn-send-gift'
                            onClick={this.handleSendGift}
                            disabled={!selectedGiftId || !token || !isPlayingVideo}
                        >
                            {getMultiLangText("Txt_Botton_01")}
                        </button>
                    </footer>
                </div>
            </div>
        );
    }
}

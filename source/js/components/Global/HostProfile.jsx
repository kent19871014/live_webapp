import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { openHostProfile } from 'actions/app'
import FlipNumbers from 'react-flip-numbers';

@connect(state => ({
    accountInfo: state.app.get("accountInfo"),
    settings: state.app.get('settings'),
    hostAccount: state.host.get('hostAccount'),
    isPlayingVideo: state.app.get('isPlayingVideo'),
    hostProfile: state.host.get('hostProfile'),
    allGiftList: state.gift.get("allGiftList"),
    hostGiftAmount: state.gift.get("hostGiftAmount"),
}))

export default class HostProfile extends Component {
    static propTypes = {
        hostAccount: PropTypes.object,
    }

    static defaultProps = {
        hostAccount: {}
    }

    handleProfile = () => {
        const { dispatch } = this.props;
        dispatch(openHostProfile(true));
    }

    countGiftAmount = (tag) => {
        const { allGiftList, hostGiftAmount } = this.props;
        const regex = new RegExp(tag, "g");

        let amount = 0;
        allGiftList.filter(item => item.get("tags").join(",").match(regex)).map((allGift) => {
            hostGiftAmount.filter(item => item.get("giftId") === allGift.get("giftId")).map((hostGift) => {
                amount += hostGift.get("giftAmount");
            })
        })

        return amount;
    }

    render() {
        const { hostAccount, hostProfile, isPlayingVideo, accountInfo, settings } = this.props;
        const {
            productId, lang,
        } = accountInfo.toJSON();

        const { imgUrl = {def: ""}, content } = hostProfile ? hostProfile.toJSON() : {};
        const { head } = imgUrl;
        const { def } = settings.get("lang").toJSON();
        const { name } = content[lang] || (content[def] || {});

        return (
            <div className='host-profile' onClick={this.handleProfile} style={{display: (!hostAccount || !isPlayingVideo) ? "none" : "block"}}>
                <div className="host-info">
                    <img className='host-photo' src={head[productId] ? head[productId] : head.def} />
                    <section className="content">
                        <p className='host-name'>{name}</p>
                        <p className="like">
                            <p className="icon icon-heart-full"></p>
                            <p className="number">{this.countGiftAmount("free")}</p>
                        </p>
                    </section>
                </div>
            </div>
        );
    }
}

import React, { Component } from 'react';
import { getMultiLangTextUI } from "../../helpers/multiLang"
import { connect } from 'react-redux';

@connect(state => ({
    stringTable: state.app.get('stringTable'),
}))

export default class FireDiceTurboRule extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { productId } = this.props;

        return (
            <div className="wrapper-container">
                <div className="header"><span>{getMultiLangTextUI("Web_FDTE_000002")}</span></div>
                <div className="wrapper-body scrollbar-style">
                    <div className="content">
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Web_FDTE_000003")}</div>
                            <div className="detail">{getMultiLangTextUI("Web_FDTE_000004")}</div>
                        </div>
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Web_FDTE_000005")}</div>
                            <div className="detail">{getMultiLangTextUI("Web_FDTE_000006")}</div>
                        </div>
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Web_FDTE_000007")}</div>
                            <div className="detail">{getMultiLangTextUI("Web_FDTE_000008")}</div>
                        </div>
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Web_FDTE_000009")}</div>
                            <div className="detail">{getMultiLangTextUI("Web_FDTE_000010")}</div>
                            <div className="detail">
                                <ul>
                                    <li>{getMultiLangTextUI("Web_FDTE_000011")}</li>
                                    <li>{getMultiLangTextUI("Web_FDTE_000012")}</li>
                                    <li>{getMultiLangTextUI("Web_FDTE_000013")}</li>
                                    <li>{getMultiLangTextUI("Web_FDTE_000014")}</li>
                                    <li>{getMultiLangTextUI("Web_FDTE_000015")}</li>
                                    <br />
                                    <li>{getMultiLangTextUI("Web_FDTE_000016")}</li>
                                    <li>{getMultiLangTextUI("Web_FDTE_000017")}</li>
                                    <li>{getMultiLangTextUI("Web_FDTE_000018")}</li>
                                    <li>{getMultiLangTextUI("Web_FDTE_000019")}</li>
                                </ul>
                            </div>
                            <div className="detail">{getMultiLangTextUI("Web_FDTE_000020")}</div>
                            <div className="bet-setion" style={{ "backgroundImage": "url('./assets/img/common/" + productId + "/bet-section.svg')" }}></div>
                            <div className="detail">
                                <ul>
                                    <li>{getMultiLangTextUI("Web_FDTE_000021")}</li>
                                    <li>{getMultiLangTextUI("Web_FDTE_000022")}</li>
                                    <li>{getMultiLangTextUI("Web_FDTE_000023")}</li>
                                    <li>{getMultiLangTextUI("Web_FDTE_000024")}</li>
                                    <li>{getMultiLangTextUI("Web_FDTE_000025")}</li>
                                    <li>{getMultiLangTextUI("Web_FDTE_000026")}</li>
                                    <li>{getMultiLangTextUI("Web_FDTE_000027")}</li>
                                </ul>
                            </div>
                        </div>
                        <div className="block">
                            <div className="note">{getMultiLangTextUI("Web_FDTE_000028")}</div>
                            <div className="note">{getMultiLangTextUI("Web_FDTE_000029")}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

import React, { Component } from 'react';
import { getMultiLangTextUI } from '../../../helpers/multiLang';
import { connect } from 'react-redux';
import PayLine from './PayLine';
import SplitLine from './SplitLine';

@connect(state => ({
	stringTable: state.app.get('stringTable'),
}))

export default class JungleRule extends Component {

	render() {
		return (<div className='slot-rule Jungle wrapper-container'>
			<div className='header'><span>{getMultiLangTextUI('Web_FDTE_000002')}</span></div>
			<div className='wrapper-body scrollbar-style'>
				<div className='content' >
					<div className='paytable-section'>
						<div className='symbol-block' >
							<div className='row'>
								<div className='center-symbol special-symbol'>
									<img src='./assets/img/common/Live_Jungle/symbol-201.png' alt='' />
									<div>
										<p className='red-color'>{getMultiLangTextUI('Web_SL168_000102')}</p>
										<p><span className='count'>5</span><span className='multiplier'>10000</span></p>
										<p><span className='count'>4</span><span className='multiplier'>1000</span></p>
										<p><span className='count'>3</span><span className='multiplier'>100</span></p>
										<p><span className='count'>2</span><span className='multiplier'>10</span></p>
									</div>
								</div>
							</div>
							<div className='row'>
								<div className='explain'>
									<ul>
										<li>{getMultiLangTextUI('Web_SLJK_000072')}</li>
									</ul>
								</div>
							</div>
						</div>
						<div className='symbol-block' >
							<div className='row'>
								<div className='center-symbol special-symbol'>
									<img src='./assets/img/common/Live_Jungle/symbol-301.png' alt='' />
									<div>
										<p className='red-color'>{getMultiLangTextUI('Web_SL168_000104')}</p>
										<p><span className='count'>5</span><span className='multiplier'>300</span></p>
										<p><span className='count'>4</span><span className='multiplier'>30</span></p>
										<p><span className='count'>3</span><span className='multiplier'>3</span></p>
									</div>
								</div>
							</div>
							<div className='row'>
								<div className='explain'>
									<ul>
										<li>{getMultiLangTextUI('Web_SLSL_000082')}</li>
										<li>{getMultiLangTextUI('Web_SLSL_000085')}</li>
									</ul>
								</div>
							</div>
						</div>
						<div className='row'>
							<div className='symbol'><img src='./assets/img/common/Live_Jungle/symbol-102.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>1000</span></p>
									<p><span className='count'>4</span><span className='multiplier'>125</span></p>
									<p><span className='count'>3</span><span className='multiplier'>25</span></p>
								</div>
							</div>
							<div className='symbol'><img src='./assets/img/common/Live_Jungle/symbol-103.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>500</span></p>
									<p><span className='count'>4</span><span className='multiplier'>100</span></p>
									<p><span className='count'>3</span><span className='multiplier'>20</span></p>
								</div>
							</div>
						</div>
						<div className='row'>
							<div className='symbol'><img src='./assets/img/common/Live_Jungle/symbol-105.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>250</span></p>
									<p><span className='count'>4</span><span className='multiplier'>75</span></p>
									<p><span className='count'>3</span><span className='multiplier'>15</span></p>
								</div>
							</div>
							<div className='symbol'><img src='./assets/img/common/Live_Jungle/symbol-1.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>200</span></p>
									<p><span className='count'>4</span><span className='multiplier'>50</span></p>
									<p><span className='count'>3</span><span className='multiplier'>10</span></p>
								</div>
							</div>
						</div>
						<div className='row'>
							<div className='symbol'><img src='./assets/img/common/Live_Jungle/symbol-4.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>150</span></p>
									<p><span className='count'>4</span><span className='multiplier'>25</span></p>
									<p><span className='count'>3</span><span className='multiplier'>10</span></p>
								</div>
							</div>
							<div className='symbol'><img src='./assets/img/common/Live_Jungle/symbol-6.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>125</span></p>
									<p><span className='count'>4</span><span className='multiplier'>25</span></p>
									<p><span className='count'>3</span><span className='multiplier'>5</span></p>
								</div>
							</div>
						</div>
					</div>

					<SplitLine />


					<div className='payline-section'>
						<div className='explain'>
							<ul>
								<li>{getMultiLangTextUI('Web_SLJK_000075')}</li>
								<li>{getMultiLangTextUI('Web_SLJK_000076')}</li>
								<li>{getMultiLangTextUI('Web_SLJK_000077')}</li>
								<li>{getMultiLangTextUI('Web_SLJK_000078')}</li>
							</ul>
						</div>

						<div className='row'>
							<PayLine row={3} col={5} line={[1, 1, 1, 1, 1]} />
							<PayLine row={3} col={5} line={[0, 0, 0, 0, 0]} />
							<PayLine row={3} col={5} line={[2, 2, 2, 2, 2]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[0, 1, 2, 1, 0]} />
							<PayLine row={3} col={5} line={[2, 1, 0, 1, 2]} />
							<PayLine row={3} col={5} line={[1, 0, 1, 2, 1]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[1, 2, 1, 0, 1]} />
							<PayLine row={3} col={5} line={[0, 0, 1, 2, 2]} />
							<PayLine row={3} col={5} line={[2, 2, 1, 0, 0]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[0, 1, 1, 1, 2]} />
							<PayLine row={3} col={5} line={[2, 1, 1, 1, 0]} />
							<PayLine row={3} col={5} line={[1, 0, 0, 0, 1]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[1, 2, 2, 2, 1]} />
							<PayLine row={3} col={5} line={[1, 1, 0, 1, 2]} />
							<PayLine row={3} col={5} line={[1, 1, 2, 1, 0]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[1, 0, 1, 2, 2]} />
							<PayLine row={3} col={5} line={[1, 2, 1, 0, 0]} />
							<PayLine row={3} col={5} line={[0, 0, 1, 2, 1]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[2, 2, 1, 0, 1]} />
							<PayLine row={3} col={5} line={[0, 0, 0, 1, 2]} />
							<PayLine row={3} col={5} line={[2, 2, 2, 1, 0]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[0, 1, 2, 2, 2]} />
							<PayLine row={3} col={5} line={[2, 1, 0, 0, 0]} />
							<PayLine row={3} col={5} line={[1, 0, 0, 1, 2]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[1, 0, 0, 1, 2]} />
							<PayLine row={3} col={5} line={[1, 2, 2, 1, 0]} />
						</div>
					</div>

					<SplitLine />

					<div className='explain-section'>
						<div className='row' >
							<div className='game-explain block'>
								<img src='./assets/img/common/Live_Jungle/game-1.jpg' alt='' />
								<ul>
									<li>{getMultiLangTextUI('Web_SLJK_000079')}</li>
									<li>{getMultiLangTextUI('Web_SLJK_000080')}</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>);
	}
}

import React, { Component } from 'react';
import { getMultiLangTextUI } from '../../../helpers/multiLang';
import { connect } from 'react-redux';
import PayLine from './PayLine';
import SplitLine from './SplitLine';

@connect(state => ({
    stringTable: state.app.get('stringTable'),
}))

export default class WesternJourneyRule extends Component {

    render() {
        return (<div className='slot-rule wrapper-container'>
            <div className='header'><span>{getMultiLangTextUI('Web_FDTE_000002')}</span></div>
            <div className='wrapper-body scrollbar-style'>
                <div className='content' >
                    <div className='paytable-section'>
                        <div className='row'>
                            <div className='special-symbol'>
                                <img src='./assets/img/common/Live_WesternJourney/symbol-301.png' alt='' />
                                <div className='info'>
                                    <div className='title'>{getMultiLangTextUI('Web_SL168_000104')}</div>
                                    <div >{getMultiLangTextUI('Web_SLWJ_000061')}</div>
                                </div>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='symbol'><img src='./assets/img/common/Live_WesternJourney/symbol-101.png' alt='' />
                                <div>
                                    <p><span className='count'>5</span><span className='multiplier'>10000</span></p>
                                    <p><span className='count'>4</span><span className='multiplier'>1000</span></p>
                                    <p><span className='count'>3</span><span className='multiplier'>200</span></p>
                                    <p><span className='count'>2</span><span className='multiplier'>25</span></p>
                                </div>
                            </div>
                            <div className='symbol'><img src='./assets/img/common/Live_WesternJourney/symbol-102.png' alt='' />
                                <div>
                                    <p><span className='count'>5</span><span className='multiplier'>2000</span></p>
                                    <p><span className='count'>4</span><span className='multiplier'>500</span></p>
                                    <p><span className='count'>3</span><span className='multiplier'>100</span></p>
                                    <p><span className='count'>2</span><span className='multiplier'>10</span></p>
                                </div>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='symbol'><img src='./assets/img/common/Live_WesternJourney/symbol-103.png' alt='' />
                                <div>
                                    <p><span className='count'>5</span><span className='multiplier'>1000</span></p>
                                    <p><span className='count'>4</span><span className='multiplier'>250</span></p>
                                    <p><span className='count'>3</span><span className='multiplier'>50</span></p>
                                    <p><span className='count'>2</span><span className='multiplier'>2</span></p>
                                </div>
                            </div>
                            <div className='symbol'><img src='./assets/img/common/Live_WesternJourney/symbol-104.png' alt='' />
                                <div>
                                    <p><span className='count'>5</span><span className='multiplier'>800</span></p>
                                    <p><span className='count'>4</span><span className='multiplier'>200</span></p>
                                    <p><span className='count'>3</span><span className='multiplier'>40</span></p>
                                    <p><span className='count'>2</span><span className='multiplier'>5</span></p>
                                </div>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='symbol'><img src='./assets/img/common/Live_WesternJourney/symbol-105.png' alt='' />
                                <div>
                                    <p><span className='count'>5</span><span className='multiplier'>500</span></p>
                                    <p><span className='count'>4</span><span className='multiplier'>100</span></p>
                                    <p><span className='count'>3</span><span className='multiplier'>25</span></p>
                                    <p><span className='count'>2</span><span className='multiplier'>5</span></p>
                                </div>
                            </div>
                            <div className='symbol'><img src='./assets/img/common/Live_WesternJourney/symbol-1.png' alt='' />
                                <div>
                                    <p><span className='count'>5</span><span className='multiplier'>400</span></p>
                                    <p><span className='count'>4</span><span className='multiplier'>80</span></p>
                                    <p><span className='count'>3</span><span className='multiplier'>10</span></p>
                                </div>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='symbol'><img src='./assets/img/common/Live_WesternJourney/symbol-2.png' alt='' />
                                <div>
                                    <p><span className='count'>5</span><span className='multiplier'>300</span></p>
                                    <p><span className='count'>4</span><span className='multiplier'>50</span></p>
                                    <p><span className='count'>3</span><span className='multiplier'>10</span></p>
                                </div>
                            </div>
                            <div className='symbol'><img src='./assets/img/common/Live_WesternJourney/symbol-34.png' alt='' />
                                <div>
                                    <p><span className='count'>5</span><span className='multiplier'>200</span></p>
                                    <p><span className='count'>4</span><span className='multiplier'>25</span></p>
                                    <p><span className='count'>3</span><span className='multiplier'>5</span></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <SplitLine />


                    <div className='payline-section'>
                        <div className='explain'>
                            <ul>
                                <li>{getMultiLangTextUI('Web_SLWJ_000062')}</li>
                                <li>{getMultiLangTextUI('Web_SLWJ_000063')}</li>
                                <li>{getMultiLangTextUI('Web_SLWJ_000064')}</li>
                            </ul>
                        </div>

                        <div className='row'>
                            <PayLine row={ 3 } col={ 5 } line={ [1, 1, 1, 1, 1] } />
                            <PayLine row={ 3 } col={ 5 } line={ [0, 0, 0, 0, 0] } />
                            <PayLine row={ 3 } col={ 5 } line={ [2, 2, 2, 2, 2] } />
                        </div>
                        <div className='row'>
                            <PayLine row={ 3 } col={ 5 } line={ [0, 1, 2, 1, 0] } />
                            <PayLine row={ 3 } col={ 5 } line={ [2, 1, 0, 1, 2] } />
                            <PayLine row={ 3 } col={ 5 } line={ [1, 0, 1, 2, 1] } />
                        </div>
                        <div className='row'>
                            <PayLine row={ 3 } col={ 5 } line={ [1, 2, 1, 0, 1] } />
                            <PayLine row={ 3 } col={ 5 } line={ [0, 0, 1, 2, 2] } />
                            <PayLine row={ 3 } col={ 5 } line={ [2, 2, 1, 0, 0] } />
                        </div>
                        <div className='row'>
                            <PayLine row={ 3 } col={ 5 } line={ [0, 1, 1, 1, 2] } />
                            <PayLine row={ 3 } col={ 5 } line={ [2, 1, 1, 1, 0] } />
                            <PayLine row={ 3 } col={ 5 } line={ [1, 0, 0, 0, 1] } />
                        </div>
                        <div className='row'>
                            <PayLine row={ 3 } col={ 5 } line={ [1, 2, 2, 2, 1] } />
                            <PayLine row={ 3 } col={ 5 } line={ [1, 1, 0, 1, 2] } />
                            <PayLine row={ 3 } col={ 5 } line={ [1, 1, 2, 1, 0] } />
                        </div>
                        <div className='row'>
                            <PayLine row={ 3 } col={ 5 } line={ [1, 0, 1, 2, 2] } />
                            <PayLine row={ 3 } col={ 5 } line={ [1, 2, 1, 0, 0] } />
                            <PayLine row={ 3 } col={ 5 } line={ [0, 0, 1, 2, 1] } />
                        </div>
                        <div className='row'>
                            <PayLine row={ 3 } col={ 5 } line={ [2, 2, 1, 0, 1] } />
                            <PayLine row={ 3 } col={ 5 } line={ [0, 0, 0, 1, 2] } />
                        </div>
                    </div>

                    <SplitLine />

                    <div className='explain-section'>
                        <div className='row'>
                            <div className='special-symbol' >
                                <img src='./assets/img/common/Live_WesternJourney/symbol-201.png' alt='' />
                                <div>
                                    <p className='red-color'>{getMultiLangTextUI('Web_SL168_000102')}</p>
                                    <p><span className='count'>5</span><span className='multiplier'>1000</span></p>
                                    <p><span className='count'>4</span><span className='multiplier'>200</span></p>
                                    <p><span className='count'>3</span><span className='multiplier'>50</span></p>
                                </div>
                            </div>
                        </div>
                        <div className='row' >
                            <div className='game-explain block'>
                                <img src='./assets/img/common/Live_WesternJourney/game-1.png' alt='' />
                                <ul>
                                    <li>{getMultiLangTextUI('Web_SLWJ_000065')}</li>
                                    <li>{getMultiLangTextUI('Web_SLWJ_000066')}</li>
                                </ul>
                            </div>
                        </div>
                        <div className='row' >
                            <div className='game-explain block'>
                                <img src='./assets/img/common/Live_WesternJourney/game-2.jpg' alt='' />
                                <ul>
                                    <li>{getMultiLangTextUI('Web_SLWJ_000067')}</li>
                                    <li>{getMultiLangTextUI('Web_SLWJ_000068')}</li>
                                    <li>{getMultiLangTextUI('Web_SLWJ_000069')}</li>
                                </ul>
                            </div>
                        </div>
                        <div className='row' >
                            <div className='game-explain block'>
                                <img src='./assets/img/common/Live_WesternJourney/game-3.jpg' alt='' />
                                <ul>
                                    <li>{getMultiLangTextUI('Web_SLWJ_000070')}</li>
                                    <li>{getMultiLangTextUI('Web_SLWJ_000071')}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>);
    }
}

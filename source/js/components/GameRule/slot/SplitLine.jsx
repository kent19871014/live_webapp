import React from 'react';

const SplitLine = () => {
    const dots = [];
    for (let i = 0; i < 21; i++) {
        dots.push(
            <div className='dot' />
        );
    }

    return <div className='split-line' >{dots}</div>;
};

export default SplitLine;

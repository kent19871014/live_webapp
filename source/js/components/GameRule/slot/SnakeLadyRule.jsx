import React, { Component } from 'react';
import { getMultiLangTextUI } from '../../../helpers/multiLang';
import { connect } from 'react-redux';
import PayLine from './PayLine';
import SplitLine from './SplitLine';

@connect(state => ({
	stringTable: state.app.get('stringTable'),
}))

export default class SnakeLadyRule extends Component {

	render() {
		return (<div className='slot-rule SnakeLady wrapper-container'>
			<div className='header'><span>{getMultiLangTextUI('Web_FDTE_000002')}</span></div>
			<div className='wrapper-body scrollbar-style'>
				<div className='content' >
					<div className='paytable-section'>
						<div className='row'>
							<div className='special-symbol'>
								<img src='./assets/img/common/Live_SnakeLady/symbol-301.png' alt='' />
								<div className='info'>
									<div className='title'>{getMultiLangTextUI('Web_SL168_000102')}</div>
									<div >{getMultiLangTextUI('Web_SLSL_000081')}</div>
								</div>
							</div>
						</div>
						<div className='row'>
							<div className='special-symbol'>
								<img src='./assets/img/common/Live_SnakeLady/symbol-201.png' alt='' />
								<div className='info'>
									<div className='title'>{getMultiLangTextUI('Web_SL168_000104')}</div>
									<div >{getMultiLangTextUI('Web_SLSL_000082')}</div>
								</div>
							</div>
						</div>
						<div className='row'>
							<div className='symbol'><img src='./assets/img/common/Live_SnakeLady/symbol-101.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>5000</span></p>
									<p><span className='count'>4</span><span className='multiplier'>500</span></p>
									<p><span className='count'>3</span><span className='multiplier'>100</span></p>
								</div>
							</div>
							<div className='symbol'><img src='./assets/img/common/Live_SnakeLady/symbol-102.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>2000</span></p>
									<p><span className='count'>4</span><span className='multiplier'>300</span></p>
									<p><span className='count'>3</span><span className='multiplier'>50</span></p>
								</div>
							</div>
						</div>
						<div className='row'>
							<div className='symbol'><img src='./assets/img/common/Live_SnakeLady/symbol-103.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>1000</span></p>
									<p><span className='count'>4</span><span className='multiplier'>150</span></p>
									<p><span className='count'>3</span><span className='multiplier'>25</span></p>
								</div>
							</div>
							<div className='symbol'><img src='./assets/img/common/Live_SnakeLady/symbol-1.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>300</span></p>
									<p><span className='count'>4</span><span className='multiplier'>50</span></p>
									<p><span className='count'>3</span><span className='multiplier'>10</span></p>
								</div>
							</div>
						</div>
						<div className='row'>
							<div className='symbol'><img src='./assets/img/common/Live_SnakeLady/symbol-4.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>200</span></p>
									<p><span className='count'>4</span><span className='multiplier'>30</span></p>
									<p><span className='count'>3</span><span className='multiplier'>5</span></p>
								</div>
							</div>
							<div className='symbol'><img src='./assets/img/common/Live_SnakeLady/symbol-7.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>50</span></p>
									<p><span className='count'>4</span><span className='multiplier'>10</span></p>
									<p><span className='count'>3</span><span className='multiplier'>2</span></p>
								</div>
							</div>
						</div>
					</div>

					<SplitLine />


					<div className='payline-section'>
						<div className='explain'>
							<ul>
								<li>{getMultiLangTextUI('Web_SLSL_000083')}</li>
								<li>{getMultiLangTextUI('Web_SLSL_000084')}</li>
								<li>{getMultiLangTextUI('Web_SLSL_000085')}</li>
							</ul>
						</div>

						<div className='row'>
							<PayLine row={3} col={5} line={[1, 1, 1, 1, 1]} />
							<PayLine row={3} col={5} line={[0, 0, 0, 0, 0]} />
							<PayLine row={3} col={5} line={[2, 2, 2, 2, 2]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[0, 1, 2, 1, 0]} />
							<PayLine row={3} col={5} line={[2, 1, 0, 1, 2]} />
							<PayLine row={3} col={5} line={[1, 0, 1, 2, 1]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[1, 2, 1, 0, 1]} />
							<PayLine row={3} col={5} line={[0, 0, 1, 2, 2]} />
							<PayLine row={3} col={5} line={[2, 2, 1, 0, 0]} />
						</div>
					</div>

					<SplitLine />

					<div className='explain-section'>
						<div className='row' >
							<div className='game-explain block'>
								<img src='./assets/img/common/Live_SnakeLady/game-1.jpg' alt='' />
								<ul>
									<li>{getMultiLangTextUI('Web_SLSL_000086')}</li>
									<li>{getMultiLangTextUI('Web_SLSL_000087')}</li>
								</ul>
							</div>
						</div>
						<div className='row' >
							<div className='game-explain block'>
								<img src='./assets/img/common/Live_SnakeLady/game-2.jpg' alt='' />
								<ul>
									<li>{getMultiLangTextUI('Web_SLSL_000088')}</li>
									<li>{getMultiLangTextUI('Web_SLSL_000089')}</li>
									<li>{getMultiLangTextUI('Web_SLSL_000090')}</li>
								</ul>
							</div>
						</div>
						<div className='row' >
							<div className='game-explain block'>
								<img src='./assets/img/common/Live_SnakeLady/game-3.jpg' alt='' />
								<ul>
									<li>{getMultiLangTextUI('Web_SLSL_000091')}</li>
									<li>{getMultiLangTextUI('Web_SLSL_000092')}</li>
								</ul>
							</div>
						</div>
						<div className='row' >
							<div className='game-explain block'>
								<img src='./assets/img/common/Live_SnakeLady/game-4.jpg' alt='' />
								<ul>
									<li>{getMultiLangTextUI('Web_SLSL_000093')}</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>);
	}
}

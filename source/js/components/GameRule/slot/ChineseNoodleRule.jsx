import React, { Component } from 'react';
import { getMultiLangTextUI } from '../../../helpers/multiLang';
import { connect } from 'react-redux';
import PayLine from './PayLine';
import SplitLine from './SplitLine';

@connect(state => ({
	stringTable: state.app.get('stringTable'),
}))

export default class ChineseNoodleRule extends Component {

	render() {
		return (<div className='slot-rule ChineseNoodle wrapper-container'>
			<div className='header'><span>{getMultiLangTextUI('Web_FDTE_000002')}</span></div>
			<div className='wrapper-body scrollbar-style'>
				<div className='content' >
					<div className='paytable-section'>
						<div className='row'>
							<div className='special-symbol'>
								<img src='./assets/img/common/Live_ChineseNoodle/symbol-201.png' alt='' />
								<div className='info'>
									<div className='title'>{getMultiLangTextUI('Web_SL168_000104')}</div>
									<div >{getMultiLangTextUI('Web_SLCN_000094')}</div>
								</div>
							</div>
						</div>
						<div className='row'>
							<div className='symbol'><img src='./assets/img/common/Live_ChineseNoodle/symbol-101.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>150</span></p>
									<p><span className='count'>4</span><span className='multiplier'>50</span></p>
									<p><span className='count'>3</span><span className='multiplier'>15</span></p>
									<p><span className='count'>2</span><span className='multiplier'>5</span></p>
								</div>
							</div>
							<div className='symbol'><img src='./assets/img/common/Live_ChineseNoodle/symbol-102.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>120</span></p>
									<p><span className='count'>4</span><span className='multiplier'>36</span></p>
									<p><span className='count'>3</span><span className='multiplier'>12</span></p>
								</div>
							</div>
						</div>
						<div className='row'>
							<div className='symbol'><img src='./assets/img/common/Live_ChineseNoodle/symbol-103.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>100</span></p>
									<p><span className='count'>4</span><span className='multiplier'>30</span></p>
									<p><span className='count'>3</span><span className='multiplier'>10</span></p>
								</div>
							</div>
							<div className='symbol'><img src='./assets/img/common/Live_ChineseNoodle/symbol-104.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>80</span></p>
									<p><span className='count'>4</span><span className='multiplier'>24</span></p>
									<p><span className='count'>3</span><span className='multiplier'>8</span></p>
								</div>
							</div>
						</div>
						<div className='row'>
							<div className='symbol'><img src='./assets/img/common/Live_ChineseNoodle/symbol-1.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>50</span></p>
									<p><span className='count'>4</span><span className='multiplier'>15</span></p>
									<p><span className='count'>3</span><span className='multiplier'>5</span></p>
								</div>
							</div>
							<div className='symbol'><img src='./assets/img/common/Live_ChineseNoodle/symbol-2.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>40</span></p>
									<p><span className='count'>4</span><span className='multiplier'>12</span></p>
									<p><span className='count'>3</span><span className='multiplier'>4</span></p>
								</div>
							</div>
						</div>
						<div className='row'>
							<div className='symbol'><img src='./assets/img/common/Live_ChineseNoodle/symbol-3.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>30</span></p>
									<p><span className='count'>4</span><span className='multiplier'>9</span></p>
									<p><span className='count'>3</span><span className='multiplier'>3</span></p>
								</div>
							</div>
							<div className='symbol'><img src='./assets/img/common/Live_ChineseNoodle/symbol-4.png' alt='' />
								<div>
									<p><span className='count'>5</span><span className='multiplier'>20</span></p>
									<p><span className='count'>4</span><span className='multiplier'>6</span></p>
									<p><span className='count'>3</span><span className='multiplier'>2</span></p>
								</div>
							</div>
						</div>
					</div>

					<SplitLine />


					<div className='payline-section'>
						<div className='explain'>
							<ul>
								<li>{getMultiLangTextUI('Web_SLCN_000095')}</li>
								<li>{getMultiLangTextUI('Web_SLCN_000096')}</li>
								<li>{getMultiLangTextUI('Web_SLCN_000097')}</li>
								<li>{getMultiLangTextUI('Web_SLCN_000098')}</li>
							</ul>
						</div>

						<div className='row'>
							<PayLine row={3} col={5} line={[1, 1, 1, 1, 1]} />
							<PayLine row={3} col={5} line={[0, 0, 0, 0, 0]} />
							<PayLine row={3} col={5} line={[2, 2, 2, 2, 2]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[0, 1, 2, 1, 0]} />
							<PayLine row={3} col={5} line={[2, 1, 0, 1, 2]} />
							<PayLine row={3} col={5} line={[1, 0, 1, 2, 1]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[1, 2, 1, 0, 1]} />
							<PayLine row={3} col={5} line={[0, 0, 1, 2, 2]} />
							<PayLine row={3} col={5} line={[2, 2, 1, 0, 0]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[0, 1, 1, 1, 2]} />
							<PayLine row={3} col={5} line={[2, 1, 1, 1, 0]} />
							<PayLine row={3} col={5} line={[1, 0, 0, 0, 1]} />
						</div>
						<div className='row'>
							<PayLine row={3} col={5} line={[1, 2, 2, 2, 1]} />
							<PayLine row={3} col={5} line={[1, 1, 0, 1, 2]} />
							<PayLine row={3} col={5} line={[1, 1, 2, 1, 0]} />
						</div>
					</div>

					<SplitLine />

					<div className='explain-section'>
						<div className='row' >
							<div className='game-explain block'>
								<img src='./assets/img/common/Live_ChineseNoodle/game-1.jpg' alt='' />
								<ul>
									<li>{getMultiLangTextUI('Web_SLCN_000099')}</li>
								</ul>
								<p className="free-spin">{getMultiLangTextUI('Web_SLCN_000111')} </p>

								<ul>
									<li>{getMultiLangTextUI('Web_SLCN_000100')}</li>
									<li>{getMultiLangTextUI('Web_SLCN_000101')}</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>);
	}
}

import React from 'react';
import classNames from 'classnames';

const genArray = (val) => {
    const arr = [];
    for (let i = 0; i < val; i++) {
        arr.push(1);
    }
    return arr;
};

const PayLine = ({ ...props }) => {
    const rows = genArray(props.row);
    const cols = genArray(props.col);
    const line = props.line;
    const table = rows.map((row, rowIndex) => {
        return (<div className='row'>
            {cols.map((value, colIndex) => {
                const val = line[colIndex] === rowIndex;
                return <div className={ classNames('box', { active: val }) } />;
            })
            }
        </div>);
    });
    return <div className={ classNames('payline', '') }>{table}</div>;
};

export default PayLine;

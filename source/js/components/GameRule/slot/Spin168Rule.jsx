import React, { Component } from 'react';
import { getMultiLangTextUI } from '../../../helpers/multiLang';
import { connect } from 'react-redux';
import PayLine from './PayLine';
import SplitLine from './SplitLine';

@connect(state => ({
    stringTable: state.app.get('stringTable'),
}))

export default class Spin168Rule extends Component {

    render() {
        return (<div className='slot-rule Spin168 wrapper-container'>
            <div className='header'><span>{getMultiLangTextUI('Web_FDTE_000002')}</span></div>
            <div className='wrapper-body scrollbar-style'>
                <div className='content' >
                    <div className='paytable-section'>
                        <div className='symbol-block' >
                            <div className='row'>
                                <div className='center-symbol special-symbol'>
                                    <img src='./assets/img/common/Live_Spin168/symbol-201.png' alt='' />
                                    <div>
                                        <p className='red-color'>{getMultiLangTextUI('Web_SL168_000102')}</p>
                                        <p><span className='count'>3</span><span className='multiplier'>2000</span></p>
                                    </div>
                                </div>
                            </div>
                            <div className='row'>
                                <div className='explain'>
                                    <ul>
                                        <li>{getMultiLangTextUI('Web_SL168_000103')}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className='symbol-block' >
                            <div className='row'>
                                <div className='center-symbol special-symbol'>
                                    <img src='./assets/img/common/Live_Spin168/symbol-301.png' alt='' />
                                    <div>
                                        <p className='red-color'>{getMultiLangTextUI('Web_SL168_000104')}</p>
                                        <p><span className='multiplier'>10/20/40</span></p>
                                    </div>
                                </div>
                            </div>
                            <div className='row'>
                                <div className='explain'>
                                    <ul>
                                        <li>{getMultiLangTextUI('Web_SL168_000105')}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='symbol'><img src='./assets/img/common/Live_Spin168/symbol-1.png' alt='' />
                                <div>
                                    <p><span className='count'>3</span><span className='multiplier'>200</span></p>
                                </div>
                            </div>
                            <div className='symbol'><img src='./assets/img/common/Live_Spin168/symbol-2.png' alt='' />
                                <div>
                                    <p><span className='count'>3</span><span className='multiplier'>50</span></p>
                                </div>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='symbol'><img src='./assets/img/common/Live_Spin168/symbol-3.png' alt='' />
                                <div>
                                    <p><span className='count'>3</span><span className='multiplier'>20</span></p>
                                </div>
                            </div>
                            <div className='symbol'><img src='./assets/img/common/Live_Spin168/symbol-4.png' alt='' />
                                <div>

                                    <p><span className='count'>3</span><span className='multiplier'>10</span></p>
                                </div>
                            </div>
                        </div>
                        <div className='row'>
                            <div className='symbol'><img src='./assets/img/common/Live_Spin168/symbol-5.png' alt='' />
                                <div>
                                    <p><span className='count'>3</span><span className='multiplier'>5</span></p>
                                </div>
                            </div>
                            <div className='symbol'><img src='./assets/img/common/Live_Spin168/symbol-6.png' alt='' />
                                <div>
                                    <p><span className='count'>3</span><span className='multiplier'>5</span></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <SplitLine />


                    <div className='payline-section'>
                        <div className='explain'>
                            <ul>
                                <li>{getMultiLangTextUI('Web_SL168_000106')}</li>
                                <li>{getMultiLangTextUI('Web_SL168_000107')}</li>
                                <li>{getMultiLangTextUI('Web_SL168_000108')}</li>
                            </ul>
                        </div>

                        <div className='row'>
                            <PayLine row={3} col={3} line={[0, 0, 0]} />
                            <PayLine row={3} col={3} line={[1, 1, 1]} />
                            <PayLine row={3} col={3} line={[2, 2, 2]} />
                        </div>
                        <div className='row'>
                            <PayLine row={3} col={3} line={[2, 1, 0]} />
                            <PayLine row={3} col={3} line={[0, 1, 2]} />
                            <PayLine row={3} col={3} line={[0, 1, 0]} />
                        </div>
                        <div className='row'>
                            <PayLine row={3} col={3} line={[2, 1, 2]} />
                            <PayLine row={3} col={3} line={[1, 0, 1]} />
                            <PayLine row={3} col={3} line={[1, 2, 1]} />
                        </div>
                    </div>

                    <SplitLine />

                    <div className='explain-section'>
                        <div className='row' >
                            <div className='game-explain block'>
                                <img src='./assets/img/common/Live_Spin168/game-1.jpg' alt='' />
                                <ul>
                                    <li>{getMultiLangTextUI('Web_SL168_000109')}</li>
                                    <li>{getMultiLangTextUI('Web_SL168_000110')}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>);
    }
}

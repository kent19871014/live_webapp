import React, { Component } from 'react';
import { getMultiLangTextUI } from "../../helpers/multiLang"
import { connect } from 'react-redux';

@connect(state => ({
    stringTable: state.app.get('stringTable'),
}))

export default class FireDiceDelux extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { productId } = this.props;

        return (
            <div className="wrapper-container">
                <div className="header"><span>{getMultiLangTextUI("Web_FDD_000117")}</span></div>
                <div className="wrapper-body scrollbar-style">
                    <div className="content">
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Web_FDD_000118")}</div>
                            <div className="detail">{getMultiLangTextUI("Web_FDD_000119")}</div>
                        </div>
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Web_FDD_000120")}</div>
                            <div className="detail">{getMultiLangTextUI("Web_FDD_000121")}</div>
                        </div>
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Web_FDD_000122")}</div>
                            <div className="detail">{getMultiLangTextUI("Web_FDD_000123")}</div>
                        </div>
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Web_FDD_000124")}</div>
                            <div className="detail">{getMultiLangTextUI("Web_FDD_000125")}</div>
                            <div className="detail">
                                <ul>
                                    <li>{getMultiLangTextUI("Web_FDD_000126")}</li>
                                    <li>{getMultiLangTextUI("Web_FDD_000127")}</li>
                                    <li>{getMultiLangTextUI("Web_FDD_000128")}</li>
                                    <br />
                                    <li>{getMultiLangTextUI("Web_FDD_000129")}</li>
                                    <li>{getMultiLangTextUI("Web_FDD_000130")}</li>
                                    <li>{getMultiLangTextUI("Web_FDD_000131")}</li>
                                </ul>
                            </div>
                            <div className="detail">{getMultiLangTextUI("Web_FDD_000132")}</div>
                            <div className="bet-setion" style={{ "backgroundImage": "url('./assets/img/common/" + productId + "/bet-section.svg')" }}></div>
                            <div className="detail">
                                <ul>
                                    <li>{getMultiLangTextUI("Web_FDD_000133")}</li>
                                    <li>{getMultiLangTextUI("Web_FDD_000134")}</li>
                                    <li>{getMultiLangTextUI("Web_FDD_000135")}</li>
                                    <li>{getMultiLangTextUI("Web_FDD_000136")}</li>
                                </ul>
                            </div>
                        </div>
                        <div className="block">
                            <div className="note">{getMultiLangTextUI("Web_FDD_000137")}</div>
                            <div className="note">{getMultiLangTextUI("Web_FDD_000138")}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

import React, { Component } from 'react';
import { getMultiLangTextUI } from "../../helpers/multiLang"
import { connect } from 'react-redux';

@connect(state => ({
    stringTable: state.app.get('stringTable'),
}))

export default class Live_QuadrupleDice extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { productId } = this.props;

        return (
            <div className="wrapper-container">
                <div className="header"><span>{getMultiLangTextUI("Web_FDDE_000030")}</span></div>
                <div className="wrapper-body scrollbar-style">
                    <div className="banner"></div>
                    <div className="slogan"><span>{getMultiLangTextUI("Web_FDDE_000031")}</span></div>
                    <div className="content">
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Web_FDDE_000032")}</div>
                            <div className="detail">{getMultiLangTextUI("Web_FDDE_000033")}</div>
                        </div>
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Web_FDDE_000034")}</div>
                            <div className="detail">{getMultiLangTextUI("Web_FDDE_000035")}</div>
                        </div>
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Web_FDDE_000036")}</div>
                            <div className="detail">{getMultiLangTextUI("Web_FDDE_000037")}</div>
                        </div>
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Web_FDDE_000038")}</div>
                            <div className="detail">{getMultiLangTextUI("Web_FDDE_000039")}</div>
                            <div className="detail">
                                <ul>
                                    <li>{getMultiLangTextUI("Web_FDDE_000040")}</li>
                                    <li>{getMultiLangTextUI("Web_FDDE_000041")}</li>
                                    <li>{getMultiLangTextUI("Web_FDDE_000042")}</li>
                                    <li>{getMultiLangTextUI("Web_FDDE_000043")}</li>
                                    <br />
                                    <li>{getMultiLangTextUI("Web_FDDE_000044")}</li>
                                    <li>{getMultiLangTextUI("Web_FDDE_000045")}</li>
                                    <li>{getMultiLangTextUI("Web_FDDE_000046")}</li>
                                    <li>{getMultiLangTextUI("Web_FDDE_000047")}</li>
                                    <li>{getMultiLangTextUI("Web_FDDE_000048")}</li>
                                </ul>
                            </div>
                            <div className="detail">{getMultiLangTextUI("Web_FDDE_000049")}</div>
                        </div>
                        <div className="block bonus">
                            <div className="title">{getMultiLangTextUI("Web_FDDE_000050")}</div>
                            <div className="detail">{getMultiLangTextUI("Web_FDDE_000051")}</div>
                        </div>
                        <div className="block">
                            <div className="bet-setion" style={{ "backgroundImage": "url('./assets/img/common/" + productId + "/bet-section.svg')" }}></div>
                            <div className="detail">
                                <ul>
                                    <li>{getMultiLangTextUI("Web_FDDE_000052")}</li>
                                    <li>{getMultiLangTextUI("Web_FDDE_000053")}</li>
                                    <li>{getMultiLangTextUI("Web_FDDE_000054")}</li>
                                    <li>{getMultiLangTextUI("Web_FDDE_000055")}</li>
                                    <li>{getMultiLangTextUI("Web_FDDE_000056")}</li>
                                    <li>{getMultiLangTextUI("Web_FDDE_000057")}</li>
                                    <li>{getMultiLangTextUI("Web_FDDE_000058")}</li>
                                </ul>
                            </div>
                        </div>
                        <div className="block">
                            <div className="note">{getMultiLangTextUI("Web_FDDE_000059")}</div>
                            <div className="note">{getMultiLangTextUI("Web_FDDE_000060")}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

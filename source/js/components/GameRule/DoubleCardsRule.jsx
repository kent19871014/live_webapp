import React, { Component } from 'react';
import { getMultiLangTextUI } from "../../helpers/multiLang"
import { connect } from 'react-redux';

@connect(state => ({
    stringTable: state.app.get('stringTable'),
}))

export default class DoubleCardsRule extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { productId } = this.props;

        return (
            <div className="wrapper-container">
                <div className="header"><span>{getMultiLangTextUI("Rul_DCT_02")}</span></div>
                <div className="wrapper-body scrollbar-style">
                    <div className="banner"></div>
                    <div className="slogan"><span>{getMultiLangTextUI("Rul_DCT_03")}</span></div>
                    <div className="content">
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Rul_DCT_04")}</div>
                            <div className="detail">{getMultiLangTextUI("Rul_DCT_05")}</div>
                        </div>
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Rul_DCT_06")}</div>
                            <div className="detail">{getMultiLangTextUI("Rul_DCT_07")}</div>
                        </div>
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Rul_DCT_08")}</div>
                            <div className="detail">{getMultiLangTextUI("Rul_DCT_09")}</div>
                        </div>
                        <div className="block">
                            <div className="title">{getMultiLangTextUI("Rul_DCT_10")}</div>
                            <div className="detail">{getMultiLangTextUI("Rul_DCT_11")}</div>
                            <div className="detail">{getMultiLangTextUI("Rul_DCT_12")}</div>
                            <div className="detail">{getMultiLangTextUI("Rul_DCT_13")}</div>
                            <div className="bet-setion" style={{ "backgroundImage": "url('./assets/img/common/" + productId + "/bet-section.svg')" }}></div>
                        </div>
                        <div className="block bonus">
                            <div className="title">{getMultiLangTextUI("Rul_DCT_14")}</div>
                            <div className="detail">{getMultiLangTextUI("Rul_DCT_15")}</div>
                        </div>
                        <div className="block">
                            <div className="note">{getMultiLangTextUI("Rul_DCT_16")}</div>
                            <div className="note">{getMultiLangTextUI("Rul_DCT_17")}</div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

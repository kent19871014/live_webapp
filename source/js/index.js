import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import {
    Provider
} from 'react-redux';

import Routes from 'routes';
import {
    loadingComplete,
    reconnectGame,
    fetchCurrencyConfig,
    updateGameSize,
} from './actions/app.js';
import {
    getQueryStringParams
} from "./helpers/queryString";
import {
    setAccountInfo,
    fetchStringTable,
    fetchSettings,
    fetchBadWordsTable,
    fetchMusicList,
} from "./actions/app";

// Load SCSS
import '../scss/app.scss';

import store from "./store";
import {
    initBadWords
} from "./helpers/badWords";

/**
 * communicate with iframe game client
 */
window.ifrAction = (type, param) => {
    switch (type) {
        case "Error":
        case "LoadingCompleted":
            const {
                app
            } = store.getState();
            const {
                isReconnect, isMuteSounds, isMuteGameMusic
            } = app.toJSON();
            store.dispatch(loadingComplete({
                isReconnect,
                isMuteSounds,
                isMuteGameMusic
            }));
            break;
        case "Reconnect":
            store.dispatch(reconnectGame());
            break;
        case "updateGameSize":
            store.dispatch(updateGameSize(param));
            break;
    }
}

store.dispatch(fetchSettings())
    .then(() => {
        let urlParam = getQueryStringParams(location.search);
        const {
            lang
        } = store.getState().app.get("settings").toJSON();
        const {
            def,
            support
        } = lang;

        if (support.indexOf(urlParam.lang) === -1) {
            urlParam.lang = def;
        }

        // according game id to load css file
        System.import('../scss/theme/' + urlParam.gameid + '.scss');

        document.body.classList.add(urlParam.gameid);

        return store.dispatch(setAccountInfo(urlParam));
    }).then(() => {
        return store.dispatch(fetchStringTable());
    }).then(() => {
        return store.dispatch(fetchMusicList());
    }).then(() => {
        const {
            badwordsUrl
        } = store.getState().app.get("settings").toJSON();
        return store.dispatch(fetchBadWordsTable(badwordsUrl));
    }).then(() => {
        return store.dispatch(fetchCurrencyConfig());
    }).then(() => {
        initBadWords();
        // Render it to DOM
        ReactDOM.render( <
            Provider store = {
                store
            } >
            <
            Routes / >
            <
            /Provider>,
            document.getElementById('root')
        );
    });

import jwt from "jwt-decode";
import giftApi from '../api/gift';
import api from '../api/index';
import boostApi from '../api/boost';
import giftError from "../error/giftError";
import {
    ASYNC_START,
    ASYNC_ERROR,
    ASYNC_SUCCESS,

    // gift
    FETCH_GIFT_LIST,
    SEND_GIFT,
    RECEIVE_GIFT,
    INIT_COLLECT,
    CALC_COLLECT,
    PLAY_GIFT_ANIMATION,
    REMOVE_RECEIVE_GIFT,
    SELECT_GIFT,
    REMOVE_GREETING,
    FETCH_BACKPACK,
    BACKPACK_UPDATE,
    CURRENT_GIFT_TAB,

    // iframe
    IFR_LOADING_COMPLETED,
    IFR_RECONNECT_GAME,
    IFR_UPDATE_GAME_SIZE,
    IFR_UPDATE_GAME_HISTORY_URL,
    IFR_UPDATE_GAME_RULE_URL,

    // video
    VIDEO_PLAYING,
    OPEN_SOUNDS,
    OPEN_MUSIC,
    MUTE_GAME_MUSIC,

    START_GAME,
    SETTINGS,
    STRING_TABLE,
    BAD_WORDS_TABLE,
    SET_ACCOUNT_INFO,
    SCREEN_RESIZE,

    // error
    USER_ERROR_MESSAGE,
    SYS_ERROR_MESSAGE,

    // chat
    SEND_MSG,
    CONNECT_CHAT_SERVER,
    SAVE_NICKNAME,
    JOIN_GROUP_CHAT_ROOM,

    // boost
    DISPLAY_BOOST_RULE,
    BOOST_UPDATE,
    SET_BOOST_REWARD,
    INIT_BOOST,

    // currency
    FETCH_CURRENCY_CONFIG,

    // music
    FETCH_MUSIC_LIST,

    // host
    INIT_HOST_ACCOUNT,
    INIT_HOST_PROFILE,
    OPEN_HOST_PROFILE,
} from './type';

const asyncStart = () => {
    return {
        type: ASYNC_START,
    };
}

const asyncSuccess = () => {
    return {
        type: ASYNC_SUCCESS,
    };
}

const asyncError = error => {
    return {
        type: ASYNC_ERROR,
        error,
    };
}

/**
 * emit event to iframe
 * @param {string} eventName
 * @param {any} param
 */
export const sendEvent2Game = (eventName, param) => {
    const iframe = document.getElementsByTagName("iframe")[0];
    const {
        contentWindow
    } = iframe;

    const {
        BWGame,
        liveEvent
    } = contentWindow;

    // bw game emitter
    if (BWGame) {
        BWGame.Common.emitter.event(eventName, param);
    } else if (liveEvent) { // thirdparty game
        liveEvent(eventName, param);
    }
}

/**
 * set account info
 * @param {Object} data
 */
export const setAccountInfo = (data) => dispatch => {
    const {
        Account: account = "",
        token = "",
        lang = "zh-cn",
        brand: brandId = "",
        currency = "USD",
        licensee = "",
        gamename: gameName = "",
        gameid = "Live_FireDice",
    } = data;

    const {
        externalID: externalId = "",
        platform = "",
        preferred_username = "",
        sub: userId = ""
    } = token ? jwt(token) : {};

    return new Promise((resolve) => {
        dispatch({
            type: SET_ACCOUNT_INFO,
            data: {
                token,
                account,
                lang,
                brandId,
                externalId,
                currency: currency.toUpperCase(),
                platform,
                licensee,
                gameName,
                preferred_username,
                productId: gameid,
                userId,
            }
        })
        resolve();
    })
}

/**
 * gift message websocket
 * @param {Object} param
 */
export const wsGiftMsg = param => {
    return giftApi.wsGiftMsg(param);
}

/**
 * send gift api
 * @param {Object} data
 */
export const sendGift = (data) => dispatch => {
    return giftApi.sendGift(data)
        .then((res) => {
            dispatch({
                type: SEND_GIFT,
                data,
            });
        })
        .catch((error) => {
            dispatch(asyncError(error));
            const {
                status,
                data
            } = error.response;
            const {
                errcode,
                errmsg
            } = data;
            const errorInfo = giftError({
                errmsg,
                errcode,
                httpcode: status,
            });

            if (errcode === 3001) {
                dispatch(userErrorMsg(errorInfo));
            } else {
                dispatch(sysErrorMsg(errorInfo));
            }
        });
};

/**
 * receive gift message
 * @param {Object} data
 */
export const receiveGift = (data) => dispatch => {
    return dispatch({
        type: RECEIVE_GIFT,
        data,
    })
}

/**
 * gift list tab (gift / backpack)
 * @param {Object} data
 */
export const setCurrentGiftTab = (data) => dispatch => {
    return dispatch({
        type: CURRENT_GIFT_TAB,
        data,
    })
}

/**
 * gift amount calculate
 * @param {Object} data
 */
export const calcCollect = (data) => dispatch => {
    return dispatch({
        type: CALC_COLLECT,
        data,
    })
}

/**
 * play gift animation
 * @param {Object} data
 */
export const playGiftAnimation = (data) => dispatch => {
    return dispatch({
        type: PLAY_GIFT_ANIMATION,
        data,
    })
}

/**
 * remove gift message from gift animation queue when the animation play completed
 * @param {Object} data
 */
export const removeReceiveGift = (data) => dispatch => {
    return dispatch({
        type: REMOVE_RECEIVE_GIFT,
        data
    });
};

/**
 * fetch all gift list api
 */
export const fetchGiftList = () => dispatch => {
    return giftApi.fetchGiftList()
        .then(data => {
            dispatch(asyncSuccess());

            return dispatch({
                type: FETCH_GIFT_LIST,
                data,
            });
        })
        .catch(error => dispatch(asyncError(error)));
};

/**
 * keep current selected gift id
 * @param {string} giftId
 */
export const selectGift = giftId => dispatch => {
    return dispatch({
        type: SELECT_GIFT,
        data: giftId,
    });
};

/**
 * query current user backpack api
 */
export const fetchBackPack = () => dispatch => {
    return giftApi.fetchBackPack()
        .then(data => {
            dispatch(asyncSuccess());

            return dispatch({
                type: FETCH_BACKPACK,
                data,
            });
        })
        .catch(error => dispatch(asyncError(error)));
};

/**
 * send gift from backpack api
 * @param {Object} data
 */
export const sendBackPackGift = (data) => dispatch => {
    return giftApi.sendBackPackGift(data)
        .then((res) => {
            dispatch({
                type: SEND_GIFT,
                data,
            });

            dispatch({
                type: BACKPACK_UPDATE,
                data: res.data.data,
            });
        })
        .catch((error) => {
            dispatch(asyncError(error));
            const {
                status,
                data
            } = error.response;
            const {
                errcode,
                errmsg
            } = data;
            const errorInfo = giftError({
                errmsg,
                errcode,
                httpcode: status,
            });

            if (errcode === 3001) {
                dispatch(userErrorMsg(errorInfo));
            } else {
                dispatch(sysErrorMsg(errorInfo));
            }
        });
};

/**
 * clean current selected gift id
 */
export const unselectGift = () => dispatch => {
    return dispatch({
        type: SELECT_GIFT,
        data: undefined,
    });
}

/**
 * remove free gift when gift animation play completed
 * @param {Object} data
 */
export const removeGreeting = (data) => dispatch => {
    return dispatch({
        type: REMOVE_GREETING,
        data,
    });
};

/**
 * 連線聊天室
 * @param {Object} data
 */
export const connectChatServer = (data) => {
    return api.connectChatServer(data);
}

/**
 * 加入聊天室
 * @param {string} data nickname
 */
export const joinGroupChatRoom = (name) => dispatch => {
    return api.joinGroupChatRoom(name).then(() => {
        dispatch({
            type: JOIN_GROUP_CHAT_ROOM,
            data: true,
        })
    })
}

/**
 * save user nickname
 * @param {string} name
 * @param {boolean} isCache
 */
export const saveNickname = (name, isCache) => dispatch => {
    return dispatch({
        type: SAVE_NICKNAME,
        data: {
            name,
            isCache
        }
    })
}

/**
 * send message to chat room
 * @param {string} msg
 */
export const sendGroupChatMsg = (msg) => dispatch => {
    return api.sendGroupChatMsg(msg).then(() => {
        return dispatch({
            type: SEND_MSG,
            data: msg,
        });
    }).catch((error) => {
        console.log(error);
    })
}

export const loadingComplete = ({
    isReconnect,
    isMuteSounds,
    isMuteGameMusic
}) => dispatch => {
    if (isReconnect) {
        // 按下開始按鈕時，要開啟遊戲音效
        setTimeout(() => {
            sendEvent2Game("request:SoundManager.startGame", {
                isMuteSounds,
                isMuteGameMusic
            });
        }, 500)

    }
    return dispatch({
        type: IFR_LOADING_COMPLETED,
        data: false,
    })
}

/**
 * iframe game client reconnect notify
 */
export const reconnectGame = () => dispatch => {
    return dispatch({
        type: IFR_RECONNECT_GAME,
        data: true,
    })
}

/**
 * set video playing
 * @param {boolean} data
 */
export const videoPlaying = (data) => dispatch => {
    return dispatch({
        type: VIDEO_PLAYING,
        data: data,
    })
}

/**
 * request iframe game client open the sound
 * set "isStartGame" is true
 */
export const startGame = ({
    isMuteSounds,
    isMuteGameMusic
}) => dispatch => {
    // 按下開始按鈕時，要開啟遊戲音效
    sendEvent2Game("request:SoundManager.startGame", {
        isMuteSounds,
        isMuteGameMusic
    });
    return dispatch({
        type: START_GAME,
        data: true,
    })
}

/**
 * fetch config api
 */
export function fetchSettings() {
    return dispatch => {
        return api.fetchSettings().then(data => dispatch({
            type: SETTINGS,
            data: data.data,
        }))
    };
}

/**
 * fetch string table api
 */
export function fetchStringTable() {
    return dispatch => {
        return api.fetchStringTable().then(data => dispatch({
            type: STRING_TABLE,
            data: data.data,
        }))
    };
}

/**
 * fetch bad words file api
 * @param {string} url
 */
export const fetchBadWordsTable = (url) => dispatch => {
    return api.fetchBadWordsTable(url)
        .then((data) => {
            return dispatch({
                type: BAD_WORDS_TABLE,
                data: data,
            })
        })
        .catch((error) => {
            dispatch(asyncError(error))
            return Promise.resolve();
        });
}

/**
 * open gift list or chat input box or sub-menubar
 * @param {string} openType
 * @param {boolean} isOpen
 */
export const openUI = (openType, isOpen) => dispatch => {
    return dispatch({
        type: openType,
        data: isOpen,
    })
}

/**
 * query host gift amount and host profile
 * @param {Object} info
 */
export const getHostProfile = (info) => dispatch => {
    return api.getHostGiftAmount(info).then(res => {
        const {
            data
        } = res;
        dispatch({
            type: INIT_COLLECT,
            data,
        })
        dispatch({
            type: INIT_HOST_ACCOUNT,
            data: info,
        })

        return api.getHostProfile(info.externalID);
    }).then((res) => {
        dispatch({
            type: INIT_HOST_PROFILE,
            data: res.data,
        })
    })
}

/**
 * request iframe game client switch sound
 * @param {boolean} data
 */
export const openSounds = (data) => dispatch => {
    sendEvent2Game("request:SoundManager.isPlaySounds", data);

    return dispatch({
        type: OPEN_SOUNDS,
        data: data,
    });
}

/**
 * switch live music
 * @param {boolean} data
 */
export const openMusic = (data) => dispatch => {
    return dispatch({
        type: OPEN_MUSIC,
        data: data,
    });
}

/**
 * switch iframe game client music
 * @param {boolean} data
 */
export const muteGameMusic = (data) => dispatch => {
    sendEvent2Game("request:SoundManager.muteGameMusic", data);
    return dispatch({
        type: MUTE_GAME_MUSIC,
        data: data,
    });
}

/**
 * uesr operate error message
 * @param {Object} data
 */
export const userErrorMsg = (data) => dispatch => {
    return dispatch({
        type: USER_ERROR_MESSAGE,
        data: data,
    });
}

/**
 * system error message
 * @param {Object} data
 */
export const sysErrorMsg = (data) => dispatch => {
    return dispatch({
        type: SYS_ERROR_MESSAGE,
        data: data,
    });
}

/**
 * notify host profile switch
 * @param {boolean} data
 */
export const openHostProfile = (data) => dispatch => {
    return dispatch({
        type: OPEN_HOST_PROFILE,
        data: data,
    });
}

/**
 * screen resize notify
 * @param {Object} data
 */
export const screenResize = (data) => dispatch => {
    return dispatch({
        type: SCREEN_RESIZE,
        data: data,
    });
}

/**
 * set chat room server "isConnected" is true or false
 * @param {boolean} data
 */
export const isConnectedChatServer = (data) => dispatch => {
    return dispatch({
        type: CONNECT_CHAT_SERVER,
        data: data,
    });
}

/**
 * boost rule display notify
 * @param {boolean} data
 */
export const displayBoostRule = (data) => dispatch => {
    return dispatch({
        type: DISPLAY_BOOST_RULE,
        data: data,
    });
}

/**
 * connect boost server websocket
 */
export const connectBoostServer = () => {
    return boostApi.connectBoostServer();
}

export const initBoost = (data) => dispatch => {
    return dispatch({
        type: INIT_BOOST,
        data: data
    })
}

/**
 * update boost current collect power
 * @param {Object} data
 */
export const updateBoost = (data) => dispatch => {
    return dispatch({
        type: BOOST_UPDATE,
        data: data
    })
}

/**
 * set boost reward "isBoostGiftReward" is true or false
 * @param {*} data
 */
export const setBoostReward = (data) => dispatch => {
    return dispatch({
        type: SET_BOOST_REWARD,
        data: data
    })
}

/**
 * fetch currency config api
 */
export const fetchCurrencyConfig = () => dispatch => {
    return api.fetchCurrencyConfig()
        .then(data => {
            dispatch(asyncSuccess());

            return dispatch({
                type: FETCH_CURRENCY_CONFIG,
                data,
            });
        })
        .catch(error => dispatch(asyncError(error)));
};

/**
 * fetch live music list
 */
export const fetchMusicList = () => dispatch => {
    return api.fetchMusicList()
        .then(data => {
            return dispatch({
                type: FETCH_MUSIC_LIST,
                data,
            });
        })
        .catch((error) => {
            dispatch(asyncError(error))
            return Promise.resolve();
        });
};

/**
 * set game size  from iframe game client
 * @param {Object} data
 */
export const updateGameSize = (data) => dispatch => {
    return dispatch({
        type: IFR_UPDATE_GAME_SIZE,
        data: data
    })
}

/**
 * update game history url from iframe game client
 * @param {string} data
 */
export const updateGameHistoryUrl = (data) => dispatch => {
    return dispatch({
        type: IFR_UPDATE_GAME_HISTORY_URL,
        data: data
    })
}

/**
 * update game rule url from iframe game client
 * @param {string} data
 */
export const updateGameRuleUrl = (data) => dispatch => {
    return dispatch({
        type: IFR_UPDATE_GAME_RULE_URL,
        data: data
    })
}

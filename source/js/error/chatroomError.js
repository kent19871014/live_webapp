import {
    Strophe
} from "strophe.js";
import {
    getMultiLangText
} from "../helpers/multiLang"


export default (data) => {
    const {
        errcode,
    } = data;

    let key = "";

    switch (errcode) {
        case Strophe.Status.ERROR:
        case Strophe.Status.CONNFAIL:
            key = "Err_ConnectionFailed";
            break;
        case Strophe.Status.AUTHFAIL:
            key = "Err_LoginFailed";
            break;
    }

    return {
        errcode,
        txt: getMultiLangText(key),
    }
}

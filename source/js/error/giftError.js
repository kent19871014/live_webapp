import {
    getMultiLangText
} from "../helpers/multiLang"

export default (data) => {
    const {
        errcode,
        errmsg,
        httpcode
    } = data;

    let key = "";
    let code = errcode || httpcode;

    switch (httpcode) {
        case 401:
            key = "Err_Token";
            break;
        case 404:
            key = "Err_Unknow_02";
            break;
    }

    switch (errcode) {
        case 9999:
            key = "Err_Unknow_02";
            break;
        case 1002:
        case 3002:
        case 4201:
            key = "Err_Token";
            break;
        case 4000:
        case 4001:
        case 4002:
            key = "Err_GiftError_01";
            break;
        case 4401:
        case 4402:
        case 4999:
            key = "Err_SystemError1";
            break;
            // credit
        case 3001:
            key = "Err_NoMoney"
            break;
    }


    return {
        errcode: code,
        txt: getMultiLangText(key),
    }
}

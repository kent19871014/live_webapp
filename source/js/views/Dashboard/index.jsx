import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import HostProfile from 'components/Global/HostProfile';
import HostProfilePopup from 'components/Global/HostProfilePopup';
import VideoStreaming from 'components/Global/VideoStreaming';
import ChatRoom from 'components/Global/ChatRoom';
import ChatInputTextBox from 'components/Global/ChatInputTextBox';
import GiftList from 'components/Global/GiftList';
import GiftAnimation from 'components/Global/GiftAnimation';
import LoadingPage from 'components/Global/LoadingPage';
import StartPage from 'components/Global/StartPage';
import MenuBar from 'components/Global/MenuBar';
import Rule from 'components/Global/Rule';
import IframeGame from 'components/Global/IframeGame';
import IframeReconnectGame from "components/Global/IframeReconnectGame";
import GiftHistory from 'components/Global/GiftHistory';
import Greeting from 'components/Global/Greeting';
import UserErrorMessage from "components/Global/UserErrorMessage";
import SysErrorMessage from "components/Global/SysErrorMessage";
import Orientation from "components/Global/Orientation";
import Nickname from 'components/Global/Nickname';
import Boost from 'components/Global/Boost';
import BoostRule from 'components/Global/BoostRule';
import BoostSpins from 'components/Global/BoostSpins';
import { wsGiftMsg, receiveGift, calcCollect, openUI, screenResize } from 'actions/app';
import {
    OPEN_SUB_MENU_BAR,
    OPEN_CHAT_INPUT_BOX,
} from 'actions/type';
import Sys from "helpers/deviceDetect";

@connect(state => ({
    isLoading: state.app.get('isLoading'),
    settings: state.app.get('settings'),
    isStartGame: state.app.get("isStartGame"),
    isOpenSubMenuBar: state.app.get("isOpenSubMenuBar"),
    accountInfo: state.app.get("accountInfo"),
    isActive: state.boost.get("isActive"),
}))

export default class Dashboard extends Component {
    static propTypes = {
        // from react-redux connect
        dispatch: PropTypes.func,
    }

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { dispatch, accountInfo } = this.props;
        const { token } = this.parseSettings(accountInfo);
        this.installWebsocket(this.props);
        this.handleResizeDashboard();
        window.addEventListener('resize', this.handleResizeDashboard);
        if(token) {
            window.addEventListener("keyup", (event) => {
                if (event.target === document.body && event.keyCode === 13) {
                    dispatch(openUI(OPEN_CHAT_INPUT_BOX, true));
                }
            })
        }
    }

    parseSettings = settings => (settings ? settings.toJS() : {})

    /**
     * window resize handle
     */
    handleResizeDashboard = () => {
        const { dispatch } = this.props;

        if (!this.dashboard) {
            return;
        }
        setTimeout(() => {
            if (Sys.isMobile) {
                this.dashboard.style.minHeight = "unset"
                this.dashboard.style.height = window.innerHeight + "px";
            }
            else {
                this.dashboard.style.minWidth = "320px";
                this.dashboard.style.minHeight = "568px";
                this.dashboard.style.width = (this.dashboard.clientHeight * 9 / 16) + "px";
            }

            dispatch(screenResize({
                screenWidth: this.dashboard.clientWidth,
                screenHeight: this.dashboard.clientHeight
            }));
        }, 300);
    }

    /**
     * websocket gift notify
     */
    installWebsocket = () => {
        const { dispatch, accountInfo } = this.props;
        const { currency } = accountInfo.toJSON();

        wsGiftMsg().then(({ receive, closeSocket }) => {
            receive((evt) => {
                dispatch(receiveGift({
                    ...JSON.parse(evt.data),
                    currency,
                }));
                dispatch(calcCollect({
                    ...JSON.parse(evt.data)
                }));
            });
            closeSocket(this.installWebsocket);
        })
    }

    /**
     * free play layout
     */
    renderFreePlayRibbon = (lang) => {
        return (
            <div className="ribbon" lang={lang}>
                <div className="freeplay"></div>
            </div>
        )
    }

    /**
     * mask
     */
    handleMaskEvent = () => {
        const { dispatch, isOpenSubMenuBar } = this.props;
        if (isOpenSubMenuBar) {
            dispatch(openUI(OPEN_SUB_MENU_BAR, !isOpenSubMenuBar))
        }
    }

    render() {
        const { isLoading, isStartGame, isOpenSubMenuBar, accountInfo, isActive } = this.props;
        const { token, lang } = this.parseSettings(accountInfo);

        let loading
        let start;
        if (isLoading) {
            loading = <LoadingPage />;
        }
        else if (!isStartGame) {
            start = <StartPage />
        }

        let mask;
        if (isOpenSubMenuBar) {
            mask = <div className="mask" onClick={this.handleMaskEvent}></div>
        }

        let freePlayRibbon;
        if (!token) {
            freePlayRibbon = this.renderFreePlayRibbon(lang);
        }

        return (
            <div className="dashboard" ref={el => this.dashboard = el}>
                {/* <div className="debug">
                    <div></div>
                </div> */}
                <VideoStreaming />
                <HostProfile />
                {freePlayRibbon}
                {token ? <Boost /> : null}
                <IframeGame />
                <Greeting />
                <ChatRoom />
                {mask}
                <GiftList />
                <GiftAnimation />
                <GiftHistory />
                <Rule />
                <MenuBar />
                {(isActive && token) ? <BoostRule /> : null}
                <IframeReconnectGame />
                <BoostSpins />
                <Nickname />
                <ChatInputTextBox />
                <HostProfilePopup />
                <UserErrorMessage />
                <SysErrorMessage />
                {loading}
                {start}
                {Sys.isMobile ? <Orientation /> : null}
            </div>
        );
    }
}

import {
    combineReducers
} from 'redux';
import app from 'reducers/app';
import gift from 'reducers/gift';
import chat from 'reducers/chat';
import boost from 'reducers/boost';
import host from 'reducers/host';

// export default combineReducers({
//     app,
//     gift
// });

export default (state = {}, action) => {
    return {
        app: app(state.app, action, state),
        gift: gift(state.gift, action, state),
        chat: chat(state.chat, action, state),
        boost: boost(state.boost, action, state),
        host: host(state.host, action, state),
    };
};

import {
    Map,
    List
} from 'immutable';

import {
    SETTINGS,
    STRING_TABLE,
    ASYNC_START,
    ASYNC_ERROR,
    ASYNC_SUCCESS,
    IFR_LOADING_COMPLETED,
    IFR_RECONNECT_GAME,
    IFR_UPDATE_GAME_SIZE,
    IFR_UPDATE_GAME_HISTORY_URL,
    IFR_UPDATE_GAME_RULE_URL,
    VIDEO_PLAYING,
    START_GAME,
    OPEN_SUB_MENU_BAR,
    OPEN_CHAT_INPUT_BOX,
    OPEN_SOUNDS,
    OPEN_MUSIC,
    MUTE_GAME_MUSIC,
    OPEN_RULE,
    SET_ACCOUNT_INFO,
    OPEN_GIFT_HISTORY,
    USER_ERROR_MESSAGE,
    SYS_ERROR_MESSAGE,
    SCREEN_RESIZE,
    BAD_WORDS_TABLE,
    FETCH_CURRENCY_CONFIG,
    FETCH_MUSIC_LIST,
} from 'actions/type';

const initialState = Map({
    settings: undefined,
    stringTable: undefined,
    asyncLoading: false,
    asyncError: null,
    strophe: null,
    isLoading: true,
    isReconnect: false,
    isPlayingVideo: false,
    isStartGame: false,
    isOpenSubMenuBar: false,
    isOpenChatInputText: false,
    isMuteSounds: false,
    isMuteMusic: false,
    isMuteGameMusic: false,
    isOpenRule: false,
    accountInfo: Map({
        token: "",
        account: "",
        lang: "zh-cn",
        brandId: "",
        externalId: "",
        currency: "",
        platform: "",
        licensee: "mg",
        gameName: "",
    }),
    isOpenGiftHistory: false,
    userErrorMsg: Map({
        txt: "",
        errcode: -1,
    }),
    sysErrorMsg: Map({
        txt: "",
        errcode: -1,
    }),
    screenSize: Map({
        screenWidth: window.innerWidth,
        screenHeight: window.innerHeight,
    }),
    badsWordsTable: undefined,
    currencyConfig: undefined,
    musicList: List(),
});



const actionsMap = {
    [SETTINGS]: (state, action) => {
        return state.merge({
            settings: action.data,
        });
    },

    [STRING_TABLE]: (state, action) => {
        return state.merge({
            stringTable: action.data,
        });
    },

    [SET_ACCOUNT_INFO]: (state, action) => {
        let accountInfo = state.get("accountInfo").toJSON();
        Object.keys(action.data).map((key) => {
            accountInfo[key] = action.data[key];
        })
        return state.merge({
            accountInfo: accountInfo
        })
    },

    // Async general actions
    [ASYNC_START]: state => {
        return state.merge({
            asyncLoading: true,
        });
    },

    [ASYNC_ERROR]: (state, action) => {
        return state.merge({
            asyncLoading: false,
            asyncError: action.error,
        });
    },

    [ASYNC_SUCCESS]: state => {
        return state.merge({
            asyncLoading: false,
        });
    },

    [IFR_LOADING_COMPLETED]: (state, action) => {
        return state.merge({
            isLoading: action.data,
            isReconnect: false,
        })
    },

    [IFR_RECONNECT_GAME]: (state, action) => {
        return state.merge({
            isReconnect: action.data
        })
    },

    [IFR_UPDATE_GAME_SIZE]: (state, action) => {
        let settings = state.get("settings").toJSON();
        settings.gameSize = action.data;
        return state.merge({
            settings: settings,
        });
    },

    [IFR_UPDATE_GAME_HISTORY_URL]: (state, action) => {
        let settings = state.get("settings").toJSON();
        settings.historyClientUrl = action.data;
        return state.merge({
            settings: settings,
        });
    },

    [IFR_UPDATE_GAME_RULE_URL]: (state, action) => {
        let settings = state.get("settings").toJSON();
        settings.ruleUrl = action.data;
        return state.merge({
            settings: settings,
        });
    },

    [VIDEO_PLAYING]: (state, action) => {
        return state.merge({
            isPlayingVideo: action.data
        })
    },

    [START_GAME]: (state, action) => {
        return state.merge({
            isStartGame: action.data
        })
    },

    [OPEN_SUB_MENU_BAR]: (state, action) => {
        let newState = state.merge({
            isOpenSubMenuBar: action.data
        })
        // History也要一起關閉
        if (!action.data) {
            newState = newState.merge({
                isOpenGiftHistory: false
            })
        }
        return newState;
    },

    [OPEN_CHAT_INPUT_BOX]: (state, action) => {
        return state.merge({
            isOpenChatInputText: action.data
        })
    },

    [OPEN_SOUNDS]: (state, action) => {
        return state.merge({
            isMuteSounds: action.data
        })
    },

    [OPEN_MUSIC]: (state, action) => {
        return state.merge({
            isMuteMusic: action.data
        })
    },

    [MUTE_GAME_MUSIC]: (state, action) => {
        return state.merge({
            isMuteGameMusic: action.data
        })
    },

    [OPEN_RULE]: (state, action) => {
        return state.merge({
            isOpenRule: action.data
        })
    },

    [OPEN_GIFT_HISTORY]: (state, action) => {
        return state.merge({
            isOpenGiftHistory: action.data
        })
    },

    [USER_ERROR_MESSAGE]: (state, action) => {
        return state.merge({
            userErrorMsg: action.data
        })
    },

    [SYS_ERROR_MESSAGE]: (state, action) => {
        return state.merge({
            sysErrorMsg: action.data
        })
    },

    [SCREEN_RESIZE]: (state, action) => {
        return state.merge({
            screenSize: action.data
        })
    },

    [BAD_WORDS_TABLE]: (state, action) => {
        return state.merge({
            badsWordsTable: action.data
        })
    },

    [FETCH_CURRENCY_CONFIG]: (state, action) => {
        return state.merge({
            currencyConfig: action.data.data
        })
    },

    [FETCH_MUSIC_LIST]: (state, action) => {
        return state.merge({
            musicList: action.data,
        });
    },
};


export default function reducer(state = initialState, action = {}, root) {
    const fn = actionsMap[action.type];
    return fn ? fn(state, action, root) : state;
}

import uuid from "uuid";
import {
    Map,
    List
} from 'immutable';

import {
    setLocalStorage
} from "../helpers/localstorage";

import {
    FETCH_GIFT_LIST,
    SELECT_GIFT,
    REMOVE_GREETING,
    SEND_GIFT,
    PLAY_GIFT_ANIMATION,
    RECEIVE_GIFT,
    CALC_COLLECT,
    REMOVE_RECEIVE_GIFT,
    OPEN_GIFT_LIST,
    FETCH_BACKPACK,
    BACKPACK_UPDATE,
    CURRENT_GIFT_TAB,
    INIT_COLLECT,
    SET_BOOST_REWARD,
} from 'actions/type';

const initialState = Map({
    allGiftList: List(),
    giftList: List(),
    freeGiftList: List(),
    missionGiftList: List(),
    receiveGiftList: List(),
    selectedGiftId: undefined,
    isOpenGiftList: false,
    currPlayingGiftAnimation: undefined,
    isShowGiftAnimation: undefined,
    greeting: List(),
    isPlayingGiftGUID: undefined,
    backpack: List(),
    hostGiftAmount: List(),
    currTab: 0,
    displayNewGiftTip: localStorage.getItem("displayNewGiftTip") === "true",
    isBoostGiftReward: false,
});

const getRandomDelay = () => Math.max(400, parseInt(Math.random() * 1000));
const getRandomGreetingType = () => parseInt(Math.random() * 2);
const getRandomNumber = () => parseInt(Math.random() * 5) + 1;

const actionsMap = {
    [FETCH_GIFT_LIST]: (state, action) => {
        return state.merge({
            allGiftList: action.data,
            giftList: action.data.filter(jsonItem => jsonItem.tags.join(",").match(/^((?!free|Mission).)*$/g)),
            freeGiftList: action.data.filter(jsonItem => jsonItem.tags.join(",").match(/free/g)),
            missionGiftList: action.data.filter(jsonItem => jsonItem.tags.join(",").match(/Mission/g)),
        });
    },

    [SELECT_GIFT]: (state, action) => {
        return state.merge({
            selectedGiftId: action.data,
        });
    },

    [REMOVE_GREETING]: (state, action) => {
        let greeting = state.get("greeting");

        return state.merge({
            greeting: greeting.remove(action.data),
        });
    },

    [SEND_GIFT]: (state, action) => {
        return state.merge({
            selectedGiftId: undefined,
        });;
    },

    [RECEIVE_GIFT]: (state, action, root) => {
        const {
            messageKey,
            giftID
        } = action.data;
        switch (messageKey) {
            case "Boost_Message":
                let receiveGiftList = state.get("receiveGiftList");
                const userId = root.app.get("accountInfo").get("userId");
                const hostExternlID = root.host.get("hostAccount").get("externalID");

                if (!hostExternlID) {
                    return;
                }

                action.data.from = {
                    displayName: hostExternlID,
                }

                action.data.GUID = uuid();
                return state.merge({
                    receiveGiftList: action.data.to.userID === userId ? receiveGiftList.unshift(action.data) : receiveGiftList.push(action.data)
                });
            default:
                const allGiftList = state.get("allGiftList");
                const gift = allGiftList.filter(gift => gift.get('giftId') === giftID).get(0);
                // free gift
                if (gift.get("tags").indexOf("free") >= 0) {
                    const pos = getRandomNumber();
                    const speed = getRandomNumber();

                    let greeting = state.get("greeting");
                    return state.merge({
                        greeting: greeting.push({
                            pos,
                            speed,
                            src: gift.get("iconUrl"),
                            key: `greeting-${uuid()}`,
                            style: {
                                greeting: true,
                                [`x${pos}`]: true,
                                [`s${speed}`]: true,
                            }
                        })
                    });
                } else {
                    let receiveGiftList = state.get("receiveGiftList");
                    const userId = root.app.get("accountInfo").get("userId");
                    action.data.GUID = uuid();
                    return state.merge({
                        receiveGiftList: action.data.from.userID === userId ? receiveGiftList.unshift(action.data) : receiveGiftList.push(action.data)
                    });
                }
        }

        return state;
    },

    [CALC_COLLECT]: (state, action, root) => {
        if (action.data.messageKey) {
            let isHas = false;
            let hostGiftAmount = state.get("hostGiftAmount").toJSON().map((item) => {
                if (item.giftId === action.data.giftID) {
                    isHas = true;
                    item.giftAmount = action.data.giftAmount;
                }

                return item;
            })

            if (!isHas) {
                hostGiftAmount.push({
                    giftId: action.data.giftID,
                    giftAmount: action.data.giftAmount
                });
            }

            return state.merge({
                hostGiftAmount,
            })
        }
    },

    [INIT_COLLECT]: (state, action, root) => {
        return state.merge({
            hostGiftAmount: action.data,
        })
    },

    [PLAY_GIFT_ANIMATION]: state => {
        const receiveGiftList = state.get("receiveGiftList");

        if (receiveGiftList.size === 0) {
            return state;
        } else {
            return state.merge({
                currPlayingGiftAnimation: receiveGiftList.get(0),
                isPlayingGiftGUID: receiveGiftList.get(0).GUID,
            });
        }
    },

    [REMOVE_RECEIVE_GIFT]: state => {
        const isPlayingGiftGUID = state.get("isPlayingGiftGUID");
        let receiveGiftList = state.get("receiveGiftList");

        return state.merge({
            receiveGiftList: receiveGiftList.filter(list => list.GUID !== isPlayingGiftGUID),
            currPlayingGiftAnimation: undefined,
        });
    },

    [OPEN_GIFT_LIST]: (state, action) => {
        const currTab = state.get("currTab");
        if (currTab === 1) {
            setLocalStorage("displayNewGiftTip", false);
            return state.merge({
                isOpenGiftList: action.data,
                displayNewGiftTip: false,
            })
        } else {
            return state.merge({
                isOpenGiftList: action.data
            })
        }
    },

    [FETCH_BACKPACK]: (state, action) => {
        let displayNewGiftTip = false;
        const allGiftList = state.get("allGiftList");
        const backpack = state.get("backpack");
        const newBackpack = action.data.map((item) => {
            let gift = allGiftList.filter(gift => gift.get('giftId') === item.giftId).get(0);
            let backPackGift = backpack.filter(g => g.get('giftId') === item.giftId).get(0);

            if (gift) {
                gift = gift.toJSON();
                if (!displayNewGiftTip && backPackGift) {
                    displayNewGiftTip = item.giftAmount > backPackGift.get("count");
                }

                gift.count = item.giftAmount;
            }
            return gift;
        })

        if (displayNewGiftTip) {
            setLocalStorage("displayNewGiftTip", true)
        }

        return state.merge({
            backpack: newBackpack,
            displayNewGiftTip: displayNewGiftTip ? displayNewGiftTip : state.get("displayNewGiftTip")
        })
    },

    [BACKPACK_UPDATE]: (state, action) => {
        const backpack = state.get("backpack").toJSON();
        if (action.data.giftAmount <= 0) {
            let index = -1;
            backpack.find((item, i) => {
                if (item.giftId === action.data.giftId) {
                    index = i;
                    return i;
                }
            })
            backpack.splice(index, 1);

            return state.merge({
                backpack
            })

        } else {
            return state.merge({
                backpack: backpack.map((item) => {
                    if (item.giftId === action.data.giftId) {
                        item.count = action.data.giftAmount;
                    }
                    return item;
                })
            })
        }
    },

    [CURRENT_GIFT_TAB]: (state, action) => {
        if (action.data === 1) {
            setLocalStorage("displayNewGiftTip", false);
            return state.merge({
                currTab: action.data,
                displayNewGiftTip: false,
            })
        } else {
            return state.merge({
                currTab: action.data
            })
        }
    },

    [SET_BOOST_REWARD]: (state, action) => {
        return state.merge({
            isBoostGiftReward: action.data,
        });
    },
}

export default function reducer(state = initialState, action = {}, root) {
    const fn = actionsMap[action.type];
    return fn ? fn(state, action, root) : state;
}

import {
    Map,
    List
} from 'immutable';

import {
    INIT_HOST_ACCOUNT,
    INIT_HOST_PROFILE,
    OPEN_HOST_PROFILE
} from 'actions/type';

const initialState = Map({
    hostProfile: Map({
        imgUrl: {
            head: {
                def: ""
            },
            profile: {
                def: ""
            }
        },
        content: {
            "en-us": {
                name: "",
                detail: [],
                talks: ""
            },
            "zh-cn": {
                name: "",
                detail: [],
                talks: ""
            },
        }
    }),
    hostAccount: undefined,
    isOpenHostProfile: false,
});

const actionsMap = {
    [INIT_HOST_ACCOUNT]: (state, action) => {
        return state.merge({
            hostAccount: action.data
        })
    },

    [INIT_HOST_PROFILE]: (state, action) => {
        return state.merge({
            hostProfile: action.data
        })
    },

    [OPEN_HOST_PROFILE]: (state, action) => {
        return state.merge({
            isOpenHostProfile: action.data
        })
    },
}

export default function reducer(state = initialState, action = {}, root) {
    const fn = actionsMap[action.type];
    return fn ? fn(state, action, root) : state;
}

import {
    Map,
    List
} from 'immutable';
import { setLocalStorage } from "../helpers/localstorage";

import {
    RECEIVE_MSG,
    CONNECT_CHAT_SERVER,
    SAVE_NICKNAME,
    JOIN_GROUP_CHAT_ROOM,
} from '../actions/type';

const initialState = Map({
    isConnected: false,
    isJoinGroupChat: false,
    isSaveNickname: false,
    nickname: localStorage.getItem("nickname") || "",
    strophe: null,
    msgList: List(),
});

const actionsMap = {
    [RECEIVE_MSG]: (state, action) => {
        return state.update("msgList", msgList =>
            msgList.push(action.data)
        );
    },

    [CONNECT_CHAT_SERVER]: (state, action) => {
        return state.merge({
            isConnected: action.data
        })
    },

    [JOIN_GROUP_CHAT_ROOM]: (state, action) => {
        return state.merge({
            isJoinGroupChat: action.data
        })
    },

    [SAVE_NICKNAME]: (state, action) => {
        const {
            name,
            isCache
        } = action.data;

        if (isCache) {
            setLocalStorage("nickname", name);
        }
        return state.merge({
            nickname: name,
            isSaveNickname: true,
        })
    },
}

export default function reducer(state = initialState, action = {}, root) {
    const fn = actionsMap[action.type];
    return fn ? fn(state, action, root) : state;
}

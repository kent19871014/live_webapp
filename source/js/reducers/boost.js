import {
    Map,
    List
} from 'immutable';
import { setLocalStorage } from "../helpers/localstorage";

import {
    DISPLAY_BOOST_RULE,
    BOOST_UPDATE,
    SET_BOOST_REWARD,
    INIT_BOOST
} from '../actions/type';

const initialState = Map({
    isOpenRule: false,
    isActive: false,
    targetPower: 0,
    currPower: 0,
    nextPower: 0,
    isReward: false,
    isFirst: localStorage.getItem("isFirst") === null,
    rewardType: undefined,
});

const actionsMap = {
    [INIT_BOOST]: (state, action) => {
        const { isLoyaltyActive, targetPower, rewardType} = action.data;
        return state.merge({
            targetPower,
            isActive: isLoyaltyActive,
            rewardType,
        });
    },

    [DISPLAY_BOOST_RULE]: (state, action) => {
        if(action.data) {
            return state.merge({
                isOpenRule: true,
            });
        }
        else {
            setLocalStorage("isFirst", false);

            return state.merge({
                isOpenRule: false,
                isFirst: false,
            });
        }
    },

    [BOOST_UPDATE]: (state, action) => {
        const isReward = state.get("isReward");

        if(isReward){
            return state.merge({
                nextPower: action.data
            });
        }
        else {
            return state.merge({
                currPower: action.data
            });
        }
    },

    [SET_BOOST_REWARD]: (state, action) => {
        if(action.data) {
            return state.merge({
                isReward: action.data,
            });
        }
        else {
            return state.merge({
                isReward: action.data,
                currPower: state.get("nextPower")
            });
        }
    },
}

export default function reducer(state = initialState, action = {}, root) {
    const fn = actionsMap[action.type];
    return fn ? fn(state, action, root) : state;
}

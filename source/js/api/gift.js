import 'es6-promise';
import axios from 'axios';
import store from "../store";

const sendGift = ({
    giftId,
    receiverId,
}) => {

    const {
        app,
        chat,
    } = store.getState();

    const {
        token,
        licensee,
        currency,
        gameName,
        productId,
    } = app.get("accountInfo").toJSON();

    const {
        giftAPI
    } = app.get("settings").toJSON();

    const {
        host,
        sendGift
    } = giftAPI;

    const nickname = chat.get("nickname")

    return axios.post(host + sendGift.replace("{licenseeID}", licensee).replace("{giftId}", giftId).replace("{receiverId}", receiverId), {
        itemCount: 1,
        currency,
        gameName,
        productId,
        platformCode: 1,
        nickName: nickname,
    }, {
        headers: {
            "authorization": `Bearer ${token}`,
            "content-type": "application/json"
        }
    })
}

const fetchGiftList = () => {

    const {
        app
    } = store.getState();

    const {
        token,
        licensee,
        productId,
        lang,
        brandId,
    } = app.get("accountInfo").toJSON();

    const {
        giftAPI
    } = app.get("settings").toJSON();

    const {
        host,
        giftList
    } = giftAPI;

    return axios.get(host + giftList.replace("{licenseeID}", licensee).replace("{brandID}", brandId), {
        headers: {
            "authorization": `Bearer ${token}`,
            "content-type": "application/json"
        }
    }).then(response => {
        const {
            data
        } = response;
        // preloading gift image
        let url = [];
        data.hits.map((item) => {
            if(item.iconUrl) {
                url.push(axios.get(item.iconUrl));
            }

            if(item.animationUrl) {
                url.push(axios.get(item.animationUrl));
            }
        })

        url.push(axios.get(`./assets/img/${lang}/${productId}_logo.png`))
        Promise.all(url);

        return data.hits;
    }).catch(error => {
        throw Error(error);
    });
}

const wsGiftMsg = () => {
    const {
        app
    } = store.getState();

    const {
        brandId,
        currency,
        token,
        licensee,
        productId,
    } = app.get("accountInfo").toJSON();

    const {
        wsGift
    } = app.get("settings").toJSON();

    const {
        apihost,
        url,
        port,
        protocol,
    } = wsGift;

    let wsUrl = `${protocol}://${apihost}${port != undefined ? (":" + port) : ""}${url}`;

    wsUrl = wsUrl.replace(/{licenseeID}/, licensee);
    wsUrl = wsUrl.replace(/{brandID}/, brandId);
    wsUrl = wsUrl.replace(/{gameID}/, productId);
    wsUrl = wsUrl.replace(/{currency}/, currency);
    wsUrl = wsUrl.replace(/{token}/, token);

    return new Promise((resolve) => {
        const ws = new WebSocket(wsUrl);
        const receive = (cb) => {
            ws.onmessage = cb;
        }

        const closeSocket = (reconnectFunc) => {
            ws.onclose = (e) => {
                console.log('Socket is closed. Reconnect will be attempted in 15 second.', e.reason);
                setTimeout(reconnectFunc, 15000);
            };
        }

        ws.onopen = () => {
            resolve({
                receive,
                closeSocket
            });
        }

        ws.onerror = (err) => {
            console.error('Gift Socket encountered error: ', err.message, 'Closing socket');
            ws.close();
        };
    })
}

const fetchBackPack = () => {

    const {
        app
    } = store.getState();

    const {
        token,
        licensee
    } = app.get("accountInfo").toJSON();

    const {
        giftAPI
    } = app.get("settings").toJSON();
    const {
        host,
        backPack
    } = giftAPI;

    return axios.get(host + backPack.replace("{licenseeID}", licensee), {
        headers: {
            "authorization": `Bearer ${token}`,
            "content-type": "application/json"
        }
    }).then(response => {
        const {
            data
        } = response;
        return data.gift_pack;
    }).catch(error => {
        throw Error(error);
    });
}

const sendBackPackGift = ({
    giftId,
    receiverId,
}) => {
    const {
        app,
        chat,
    } = store.getState();

    const {
        token,
        licensee,
        gameName,
        productId,
        currency,
    } = app.get("accountInfo").toJSON();

    const {
        giftAPI
    } = app.get("settings").toJSON();

    const {
        host,
        sendBackPackGift
    } = giftAPI;

    const nickname = chat.get("nickname")

    return axios.post(host + sendBackPackGift.replace("{licenseeID}", licensee).replace("{missionGiftId}", giftId).replace("{receiverId}", receiverId), {
        itemCount: 1,
        currency,
        gameName,
        productId,
        nickName: nickname,
    }, {
        headers: {
            "authorization": `Bearer ${token}`,
            "content-type": "application/json"
        }
    })
}

export default {
    wsGiftMsg,
    sendGift,
    fetchGiftList,
    fetchBackPack,
    sendBackPackGift,
}

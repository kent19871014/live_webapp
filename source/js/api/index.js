import 'es6-promise';
import {
    $msg,
    $pres,
    Strophe
} from "strophe.js";
import axios from 'axios';
import {
    RECEIVE_MSG
} from "../actions/type";

import {
    cleanBadWords,
} from "../helpers/badWords";
import store from "../store";
import decodeHTML from "../helpers/decodeHTML";
import {
    unescaped2Escaped,
    escaped2Unescaped
} from "../helpers/escapingTransformations";


let stropheInfo = {};

/**
 * 連線 chat server
 */
const connectChatServer = ({
    reconnectFun,
}) => {

    const {
        app,
    } = store.getState();

    const {
        token,
        preferred_username
    } = app.get("accountInfo").toJSON();

    const {
        chatRoom
    } = app.get("settings").toJSON();

    const {
        domain,
        service,
        forFunChatName
    } = chatRoom;
    const {
        forReal,
        forFun
    } = domain;

    stropheInfo = {
        connection: new Strophe.Connection(service),
        jId: token ? (unescaped2Escaped(preferred_username) + forReal) : (forFunChatName + forFun),
        pwd: token ? token : forFunChatName,
    };

    return new Promise((resolve, reject) => {
        stropheInfo.connection.connect(stropheInfo.jId, stropheInfo.pwd, (status, condition) => {
            console.log("stropheInfo connection status = ", status)
            switch (status) {
                case Strophe.Status.CONNECTED:
                    stropheInfo.connection.send($pres().tree());
                    resolve(receiveChatRoomMessage);
                    break;
                case Strophe.Status.DISCONNECTED:
                    reconnectFun();
                    break;
                case Strophe.Status.CONNFAIL:
                case Strophe.Status.AUTHFAIL:
                    reject({
                        condition,
                        errcode: status
                    });
                    break;
                default:
                    break;
            }
        })
    })
}

/**
 * 加入群聊聊天室
 */
const joinGroupChatRoom = () => {
    const {
        app,
        chat,
    } = store.getState();

    const {
        token,
        externalId
    } = app.get("accountInfo").toJSON();

    const {
        chatRoom
    } = app.get("settings").toJSON();

    const {
        groupChatAddr,
        seconds,
        maxstanzas,
        forFunChatName,
    } = chatRoom;

    const nickname = chat.get("nickname");

    const joinRoom = () => {
        return new Promise((resolve, reject) => {
            const {
                connection,
                jId
            } = stropheInfo;
            connection.send($pres({
                from: jId,
                to: groupChatAddr + "/" + (token ? externalId : forFunChatName) + "+" + nickname,
            }).c("x", {
                xmlns: Strophe.NS.MUC
            }).c("history", {
                maxstanzas,
                seconds
            }).up().tree());
            resolve();
        })
    }
    if (token) {
        return patchNickname(nickname).then(() => {
            return joinRoom();
        })
    } else {
        return joinRoom();
    }
}

/**
 * 收到聊天室訊息
 */
const receiveChatRoomMessage = (dispatch) => {
    const {
        app,
        chat,
    } = store.getState();

    const {
        externalId
    } = app.get("accountInfo").toJSON();
    const nickname = chat.get("nickname");

    stropheInfo.connection.addHandler((msg) => {
        const from = msg.getAttribute("from");
        const type = msg.getAttribute("type");
        const elems = msg.getElementsByTagName("body");
        const name = from.substring(from.indexOf("/") + 1);
        const nickname = name.split("+")[1];

        if (type === "groupchat" && elems.length > 0) {
            const body = elems[0];
            store.dispatch({
                type: RECEIVE_MSG,
                data: {
                    player: nickname,
                    content: cleanBadWords(decodeHTML(Strophe.getText(body))),
                    isOwner: name === escaped2Unescaped((externalId + "+" + nickname))
                }
            });
        }

        return true;
    }, null, "message", null, null, null);
}

/**
 * 發送群聊訊息
 */
const sendGroupChatMsg = (msg) => {
    const {
        app,
    } = store.getState();
    const {
        lang
    } = app.get("accountInfo").toJSON();
    const {
        chatRoom
    } = app.get("settings").toJSON();

    const {
        groupChatAddr,
    } = chatRoom;
    const {
        connection,
        jId,
    } = stropheInfo;

    const content = $msg({
        from: jId,
        to: groupChatAddr,
        type: "groupchat",
    }).attrs({
        "xml:lang": lang
    }).c("body", null, msg);

    connection.send(content);

    return Promise.resolve();
}

/**
 * 取得設定檔
 */
const fetchSettings = () => {
    let promise;

    promise = axios.get('configs/settings.json').then(response => {
        return response;
    });

    return promise;
}

/**
 * 取得string table
 */
const fetchStringTable = () => {
    let promise;

    promise = axios.get('configs/Webapp_String_Dictionary.json').then(response => {
        return response;
    });

    return promise;
}

/**
 * 取得不雅字眼string table
 */
const fetchBadWordsTable = (url) => {
    let promise;

    const getMethod = (str) => {
        return axios.get(str)
    }

    promise = axios.all(url.map((item) => getMethod(item)))
        .then(axios.spread(function (baseWord, extendWord) {
            return [...baseWord.data.trim().split("\n"), ...extendWord.data.trim().split("\n")];
        }));

    return promise;
}

/**
 * patch nickname
 */
const patchNickname = (nickname) => {
    const {
        app,
    } = store.getState();
    const {
        token,
        userId,
    } = app.get("accountInfo").toJSON();
    const {
        boost,
    } = app.get("settings").toJSON();
    let promise;

    promise = axios.patch(boost.patchNickname.replace("{playerId}", userId), {
        displayName: nickname
    }, {
        headers: {
            "authorization": `Bearer ${token}`,
            "content-type": "application/json"
        }
    }).then(response => {
        return response;
    });

    return promise;
}

/**
 * 取得主播like數量
 */
const getHostGiftAmount = (data) => {
    const {
        app
    } = store.getState();
    const {
        token
    } = app.get("accountInfo").toJSON();
    const {
        giftAPI,
    } = app.get("settings").toJSON();
    const {
        host,
        hostInfo
    } = giftAPI;
    return axios.get(host + hostInfo.replace("{hostId}", data.sub), {
        headers: {
            "authorization": `Bearer ${token}`,
            "content-type": "application/json"
        }
    })
}

/**
 * 取得主播自我介紹
 */
const getHostProfile = (externalID) => {
    const {
        app
    } = store.getState();

    const {
        profileUrl
    } = app.get("settings").toJSON();

    let promise;
    promise = axios.get(profileUrl.replace(/{externalId}/g, externalID.toUpperCase())).then(response => {
        return response;
    });

    return promise;
}

const fetchCurrencyConfig = () => {
    const {
        app
    } = store.getState();
    const {
        currencyConfigAPI
    } = app.get("settings").toJSON();

    return axios.get(currencyConfigAPI, {
        headers: {
            "content-type": "application/json"
        }
    })
}

/**
 * 取得要撥放的音樂list
 */
const fetchMusicList = () => {
    const {
        app,
    } = store.getState();

    const {
        musicList
    } = app.get("settings").toJSON();

    let promise;

    promise = axios.get(musicList).then(response => {
        return response.data;
    });

    return promise;
}

export default {
    connectChatServer,
    sendGroupChatMsg,
    fetchSettings,
    fetchStringTable,
    fetchBadWordsTable,
    joinGroupChatRoom,
    getHostGiftAmount,
    getHostProfile,
    fetchCurrencyConfig,
    fetchMusicList,
};

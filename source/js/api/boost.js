import store from "../store";
import {
    initBoost,
    updateBoost,
    setBoostReward,
    fetchBackPack
} from "../actions/app"

/**
 * 連線 boost server
 */
const connectBoostServer = () => {

    const {
        app,
    } = store.getState();

    const {
        token,
        licensee,
        brandId,
        currency,
        productId,
    } = app.get("accountInfo").toJSON();

    const {
        boost
    } = app.get("settings").toJSON();

    const url = boost.qeuryWSUrl
        .replace("{licenseeID}", licensee)
        .replace("{brandID}", brandId)
        .replace("{gameID}", productId)
        .replace("{Currency}", currency) + token;

    const connect = () => {
        const ws = new WebSocket(url);

        ws.onopen = () => {
            console.log("Boost Connected");
        }

        ws.onmessage = (e) => {
            console.log('Message:', e.data);
            const data = JSON.parse(e.data);
            const {
                messageType,
                isLoyaltyActive,
                accumulatorThreshold,
                balance,
                rewardType,
            } = data;

            switch (messageType) {
                case "LoyaltyAccumulatorBalance":
                    store.dispatch(initBoost({
                        isLoyaltyActive,
                        rewardType,
                        targetPower: accumulatorThreshold,
                    }));
                case "LoyaltyAccumulatorBalanceUpdate":
                    if (balance >= accumulatorThreshold) {
                        store.dispatch(updateBoost(balance))

                        // 暫時拿掉禮物通知menu bar動畫
                        // store.dispatch(setBoostReward(true))
                    } else {
                        store.dispatch(updateBoost(balance))
                    }
                    break;
                case "LoyaltyReward":
                    // 暫時拿掉禮物通知menu bar動畫
                    // store.dispatch(setBoostReward(true))

                    store.dispatch(fetchBackPack());
                    break;
                default:
                    console.error("unknow messageType '" + messageType + "' in boost");
                    break;
            }
        };

        ws.onclose = (e) => {
            console.log('Boost Socket is closed. Reconnect will be attempted in 15 second.', e.reason);
            setTimeout(() => {
                connect();
            }, 15000);
        };

        ws.onerror = (err) => {
            console.error('Socket encountered error: ', err.message, 'Closing socket');
            ws.close();
        };
    }

    connect();
}

export default {
    connectBoostServer,
};
